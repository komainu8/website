---
layout: default
main_title: ブラウザカスタマイズ
sub_title:
type: service
---

クリアコードでは多種多様なブラウザに関するお客様のご要望にお応えします。
まずは、「セキュリティを高めたい」、「ユーザの負担を軽減したい」、「パフォーマンスを改善したい」など現状の課題をお知らせいただければ、その解決方法をお客様とともに検討します。

以下、ブラウザ関連の対応実績をプラットフォームや目的別に紹介します。

## 専用端末向け（PC）

PCベースの専用端末において、特定業務専用に特化させたブラウザをFirefoxベースで開発し提供します。
また複数ブラウザを併用する場合は、ブラウザ切替ツールBrowserSelectorもあわせて提供しています。

* 金融機関向け専用システムにおけるFirefoxのサポート(2010年)
    * 古いOSを利用しなければならない環境のため、依存関係のライブラリをビルドして動作するようにしたFirefoxを提供。
 * 損害保険会社様コールセンター向けFirefoxのカスタマイズ(2013年)
    * コールセンターの専用アプリを動かすため専用のFirefoxのカスタマイズ。ウィンドサイズの固定化、各種メニューの非表示化など
 * 通信会社様コールセンター向けブラウザ切替ツール(BrowserSelector）導入(2020年)
    * コールセンターの専用アプリはGoogle Chrome、社内システムはInternet Explorerと複数ブラウザの切り替えを自動化
 * 公共機関利用者向け端末におけるFirefoxのカスタマイズ(2018年)
    * 旧システムではWindows + IEを利用、システム更改にあたりUbuntu + Firefoxに移行。
    * 専用端末で利用するアプリケーションの挙動をIE利用時に近い形にするためのFirefoxのカスタマイズ
    * ファイルのアップロード、ダウンロードを禁止するアドオンの組み込み
 * Salesforceを利用するためのFirefoxカスタマイズ（2014年）


## 企業のクライアントPC向け(2007年-)

Firefoxの企業導入とサポートはこれまでに50社以上に提供しています。

 * 中央官庁様セカンドブラウザとしてのFirefoxカスタマイズおよび導入
 * 大手インフラ企業様Firefoxおよび設定ファイルの配信サーバ構築、Firefoxカスタマイズおよび導入
 * エネルギー関連企業様FirefoxおよびBrowserSelector導入(IEとFirefox併用環境の支援)
 * 大手金融機関様Firefoxカスタマイズ、導入、ESR版のバージョンアップ支援 他
 * Firefoxのアドオンを多数開発し、OSSとして提供。以下はその一例。
     * IE View WE
        * FirefoxからIEを開くためのアドオン
        * https://addons.mozilla.org/en-US/firefox/addon/ie-view-we/
     * Upload Blocker
        * ブラウザからのファイルアップロードを禁止するアドオン
        * https://addons.mozilla.org/en-US/firefox/addon/upload-blocker/
     * Reload on Idle
        * 特定のタブを定期的にリロードするアドオン (メモリ抑止)
        * https://addons.mozilla.org/en-US/firefox/addon/reload-on-idle/
     * ime-mode
        * IMEをブラウザから切り替えるためのアドオン
        * https://addons.mozilla.org/en-US/firefox/addon/ime-mode-we/
     * Transifex Open Occurrences
        * 翻訳サイトTransifexのヘルパーアドオン
        * https://addons.mozilla.org/en-US/firefox/addon/transifex-open-occurrences/
     * ToC Generator for GitHub Wiki
        * GitHubのWikiページで目次を生成するためのアドオン
        * https://addons.mozilla.org/en-US/firefox/addon/toc-generator-for-github-wiki/
     * Unminimize Browser Window On Load
        * ページ更新時に最小化を解除するためのアドオン
        * https://addons.mozilla.org/en-US/firefox/addon/unminimize-window-on-load/
     * Merge Named Browser Windows
        * ウィンドウを自動的にマージするためのアドオン
        * https://addons.mozilla.org/en-US/firefox/addon/toc-generator-for-github-wiki/

## Webサービス向け

 * ポータルサイト向けFirefoxおよびIEのツールバー開発(2007年)
 * 動画共有サイト向けIE用プラグイン開発。Firefox向けにNAAPI版も作成。(2007年)
 * 辞書サービス向け検索プラグイン開発(2008年)
 * TV番組情報検索ためのFirefoxアドオンおよびIEプラグイン開発(2008年)
 * 医療サービス利用者向けFirefoxカスタマイズ（ノーブランド版Firefoxの作成）(2013年)
 * 大手金融機関様インターネットバンキングにおけるFirefoxのサポート(2017年)

## その他

 * Firefoxプライグインのソースコードレビューおよび高速化(2009年)
 * Android版Firefox上で特定Webアプリの動作が遅くなる問題の調査(2012年)
 * Google Chrome向け拡張機能IE View WE(WebExtensionsベースのIE Viewクローン)の開発　（2021年）

## サービスに関する問い合わせ

サービスに関するお問い合わせは、こちらの [お問い合わせフォーム](/contact/) からご連絡ください。
