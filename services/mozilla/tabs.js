(function($) {
  if (!$) return;

  $.fn.tabs = function(options) {
    options = options || {};

    var container = $(this);
    container.addClass('stylized');

    if (!options.resized) {
      $(window).resize(function() {
        container.tabs({ resized: true });
      });
    }

    var tabs = container.find('.tab');
    var blocks = $('.categorized-item');
    function updateBlocksVisibility(selectedCategory) {
      blocks.each(function() {
        var block = $(this);
        if (block.hasClass(selectedCategory)) {
          block.show();
        } else {
          block.hide();
        }
      });
    }

    var width = parseInt(container.outerWidth() / tabs.length);

    tabs.each(function(index) {
      var item = $(this);
      var tab = item.find('.tab-label');
      tab.outerWidth(width)
         .height('3em')
         .css('position', 'absolute')
         .css('top', 0)
         .css('left', width * index)
         .css('z-index', 100)
         .attr('height', tab.outerHeight());

      if (!options.resized) {
        tab.click(function() {
          var selected;
          tabs.each(function(tabIndex) {
            var item = $(this);
            var tab = item.find('.tab-label');
            var panel = item.find('.tab-panel');
            if (tabIndex == index) {
              tab.addClass('selected');
              panel.show();
              selected = item.attr('id');
            } else {
              tab.removeClass('selected');
              panel.hide();
            }
          });
          updateBlocksVisibility(selected);
        });
      }

      var panel = item.find('.tab-panel');
      panel
        .css('position', 'absolute')
        .css('top', tab.outerHeight() - 1)
        .css('left', 0)
        .css('right', 0)
        .attr('height', panel.outerHeight());

      if (!options.resized) {
        if (index) {
          panel.hide();
        } else {
          tab.addClass('selected');
        }
      }
    });

    function maxHeight() {
      var heights = [];
      tabs.each(function() {
        heights.push(Number($(this).find('.tab-label').attr('height')) +
                     Number($(this).find('.tab-panel').attr('height')));
      });
      return Math.max.apply(Math, heights);
    }

    container.height(maxHeight());

    if (!options.resized) {
      updateBlocksVisibility(tabs.first().attr('id'));
      $('tr.target').hide();
    }
  };
})(jQuery);
