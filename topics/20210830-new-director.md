---
layout: default
main_title: 新任取締役就任のお知らせ
sub_title:
type: topic
---

<div class="signature">
	<p class="person">株式会社クリアコード</p>
</div>

2021年8月30日に開催しました第15回定時株主総会において、下記の通り新任取締役が選任され、8月30日付けで就任することとなりましたのでお知らせいたします。
これを機に、社員一丸となって、フリーソフトウェアの発展と、ソフトウェア開発技術の向上により一層努めてまいる所存です。

今後とも格別のご支援を賜りますよう、よろしくお願い申し上げます。


<div class="following">
	<p class="following-mark">記</p>

<table>
  <tr>
   <th>新任取締役</th>
   <td>藤本　誠二</td>
   </tr>
</table>
</div>


<p class="afterpiece">以上</p>
