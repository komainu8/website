---
title: collectdのLuaプラグインでより多彩なフックを利用できるようになりました
author: kenhys
tags:
  - lua
---


システムやアプリケーションのメトリクス情報をあつめるソフトウェアの1つに[collectd](https://www.collectd.org/)があります。
今回はcollectdの開発版で利用可能になった、Luaプラグインに追加されたフックについて紹介します。

<!--more-->

## Luaプラグインでこれまで利用できたフック

collectdはC言語で実装されていますが、プラグインを別の言語で実装できるようになっています。
Luaだけではなく、PerlやPython、Javaが利用できます。

Luaプラグインを利用する際には、従来2つのフックのみが利用できるようになっていました。

* read
* write

Luaプラグインを利用したサンプルは次のようになります。

```lua
function read()
   -- メトリクス情報をここで収集する処理を実装してcollectdに渡す
   dispatch_values(values)
   return 0
end

function write(metrics)
   -- メトリクス情報がcollectdから渡されるので処理する
   return 0
end

collectd.register_read(read)
collectd.register_write(write)
```

これは、readフックを登録することで、collectdに渡すメトリクス情報をLuaで読み込むことができ、
writeフックを登録することでcollectdから受け取ったメトリクス情報を書き出せるということです。

以前、株式会社セナネットワークス様からの発注を受けて開発した障害復旧機能を提供するcollectdプラグイン([lua-collectd-monitor](https://github.com/clear-code/lua-collectd-monitor))では、Luaプラグインを利用して実装しました。[^lua-collectd-monitor]

[^lua-collectd-monitor]: リモートからリカバリコマンドを受け取って実行したり、ローカルのメトリクスデータをもとにリカバリコマンドを実行できます。

* [障害復旧機能を提供するcollectdプラグインの紹介(ローカル監視機能編)]({% post_url 2021-01-25-index %})
* [障害復旧機能を提供するcollectdプラグインの紹介(リモート関連機能)]({% post_url 2021-04-01-index %})

ただし、当時のcollectdのLuaプラグインでは、lua-collectd-monitorに必要なフックの機能が不足していました。
例えば、初期化のときの処理や終了時の処理を書こうと思っても、フックの種類がこれだけでは実現できません。
そのため、独自にLuaプラグインを拡張して障害復旧機能を実装していました。

## Luaプラグインでこれから利用できるようになるフック

collectdのmainブランチでは、次のフックが利用できるようになりました。

* init
  * プラグインの初期化フェーズで実行されるフック
* shutdown
  * プラグインの終了フェーズで実行されるフック
* notificaiton
  * collectdのNotificationに対応して実行されるフック

また、configフックの機能が拡張され、プラグインの設定フェーズでフックにパラメータを渡せるようになりました。

拡張されたLuaプラグインを利用したサンプルは次のようになります。

```lua
function init()
   -- プラグインの初期化処理を実装する
   return 0
end

function config(conf)
   -- collectd.confのScriptに対応した<Module>の設定が渡される
   return 0
end

function read()
   -- メトリクス情報をここで収集する処理を実装してcollectdに渡す
   dispatch_values(values)
   return 0
end

function write(metrics)
   -- メトリクス情報がcollectdから渡されるので処理する
   return 0
end

function shutdown()
   -- プラグインの終了時の後始末を実装する
   return 0
end

function notification(notif)
   -- collectdからの通知イベントを処理する
   return 0
end

collectd.register_init(init)
collectd.register_config(config)
collectd.register_read(read)
collectd.register_write(write)
collectd.register_shutdown(shutdown)
collectd.register_notification(notification)
```


### configフックのさらなる拡張

lua-collectd-monitorで独自に拡張していた部分も、mainブランチに取り込まれた段階ではより強化されています。

例えば、lua-collectd-monitor向けに独自に拡張した過去のバージョンでは、`<Module>`はKey-Valueのみ受け付けており、
例え`Script`ごとに同じ設定値であっても、それぞれの`<Module>`ごとに記述する必要がありました。

```
<Plugin lua>
  BasePath "/legacy/collectd/"
  Script "test-foo.lua"
  Script "test-bar.lua"
  <Module "test-foo.lua">
    Url "https://example.jp"
  </Module>
  <Module "test-bar.lua">
    Url "https://example.jp"
  </Module>
</Plugin>
```

今回mainブランチに取り込まれたバージョンでは、スクリプト共通のconfigと個別のconfigを設定できるようになりました。
一部だけ違う設定を適用したいときに記述が少なくてすむようになっています。

```
Script "script1.lua"
Script "script2.lua"
<Module>
  Key Value <= こっちは script1.lua と script2.luaに渡される
<Module>
<Module "script2.lua">
  Key2 Value2  <= こっちはscript2.luaにのみ渡される
</Module>
```

また、ネストしたテーブルを渡せるようになりました。

```
<Module>
  Key1 "Value1"
  <Child>
    Key2 "Value2"
  </Child>
</Module>
```

このような設定をすると、Luaプラグインには次のようなテーブルとして設定が渡されます。

`{ Key1 => "Value1", Child => { Key2 => "Value2" }}`

## まとめ

今回は、collectdのLuaプラグインでより多彩なフックを利用できるようになったことを紹介しました。
まだmainブランチのみですが、いずれ正式にリリースされるはずです。

クリアコードでは、フリーソフトウェアのプラグインの開発など、開発した成果物をあらかじめ依頼者の同意を得てできるだけ公開するようにしています。
lua-collectd-monitorもそのような成果の1つであり、collectdへの改変もアップストリームへとフィードバックしました。
独自にパッチを抱えたままだと将来的なメンテナンスコストもかさむので、フィードバックして取り込んでもらうことは依頼者にとってもメリットがあるからです。

これはクリアコードの理念である、「フリーソフトウェアとビジネスの両立」にもつながります。

正式にこの機能が入ったcollectdがリリースされたらぜひ使ってみてください。
