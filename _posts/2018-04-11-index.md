---
tags: []
title: Firefoxの法人向けカスタマイズのレシピ集の使い方
---
### はじめに

クリアコードでは、Webブラウザ「Mozilla Firefox」のサポートサービスを行っています。
また、サポート等を通じて得られた知見はククログの[Mozilla](/blog/category/mozilla.html)カテゴリにまとめています。
<!--more-->


今回は、Mozilla Firefoxの法人向けカスタマイズを通じて得られた知見をまとめたレシピ集である、「firefox-support-common」とその使い方について紹介します。

### firefox-support-commonとは

法人向けで必要とされるカスタマイズの項目とその設定をまとめたファイルから構成されています。
GitHubの[firefox-support-common](https://github.com/clear-code/firefox-support-common)リポジトリにて成果物を公開しています。

現在は次期ESRであるFirefox 60ESR向けに内容の整備を進めています。

  * https://github.com/clear-code/firefox-support-common/blob/d184155fa5f965b43abe4a6d29fbc426cce41a59/configurations/customization-items.ods

このファイルには複数のシートが存在します。

  * customization-items

  * menuitem-shortcut-disable-items

  * misc-ui-disable-items

  * verify-targets

それぞれどのような内容か説明します。

#### customization-items

よくカスタマイズで用いられる設定項目をカテゴリー別にまとめた設定シートです。
以下のような項目から構成されています。

  * カテゴリー

  * カスタマイズ項目（目的）

  * サイト別設定の可否

  * 選択肢

  * 設定方法

  * 設定内容（Firefox 60）

必要に応じてカスタマイズ内容を選択し、設定方法にしたがって設置内容を反映すればお望みのカスタマイズを実現できます。

例えば、「Firefoxの自動更新を禁止したい」という場合には、検証対象IDの「Update-1-3」を参照します。

  * カテゴリー：自動更新

  * カスタマイズ項目：Firefoxの自動更新の可否

  * サイト別設定の可否：-

  * 選択肢：禁止する

  * 設定方法：MCDで設定を反映する

これに該当する設定内容（Firefox 60）は以下の通りです。

```
lockPref("app.update.enabled", false);
lockPref("app.update.auto", false);
lockPref("app.update.mode", 0);
// Firefox自体自体の自動更新のURL
lockPref("app.update.url", "");
lockPref("app.update.url.details", "");
lockPref("app.update.url.manual", "");
```


MCD( `autoconfig.cfg` )に上記設定を反映するとよいことがわかります。インストーラーへ反映するやりかたについては別記事がありますのでそちらを参照してください。

  * [Fx Meta Installerを使った、カスタマイズ済みのFirefoxのインストーラーの作り方]({% post_url 2012-11-07-index %})

#### menuitem-shortcut-disable-items

メニューやショートカットのカスタマイズに関する設定シートです。
ただし、Firefox 60ESRでは従来アドオンで実現していた項目が多かったため、Policy Engineで設定可能なものを除いては大半が廃止となりました。

#### misc-ui-disable-items

その他非表示にしたいメニューのカスタマイズに関する設定シートです。
ただし、Firefox 60ESRでは従来アドオンで実現していた項目であったため、すべて廃止となりました。

#### verify-targets

選択したカスタマイズ内容をリストアップするためのシートです。
customization-itemsシートのA列で選択した項目が列挙されます。[^0]

### まとめ

今回は、Mozilla Firefoxの法人向けカスタマイズを通じて得られた知見をまとめたレシピ集である、「firefox-support-common」とその使い方について紹介しました。

カスタマイズが実際にきちんと反映され、意図通りになっているのかを確認するためには、検証手順に則った確認が必要です。
firefox-support-commonの設定シートを利用して検証手順書を作成することもできるのですが、これについてはまた別の機会に紹介します。

[^0]: このシートは検証手順書を作成するために活用することになります。
