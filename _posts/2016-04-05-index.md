---
tags: []
title: OSDNのファイルリリース機能をAPI経由で使うには
---
### はじめに

「オープンソース・ソフトウェア開発/公開のために様々な機能を提供する無料サービス」の1つに、[OSDN](https://osdn.jp)があります。
かつてSourceForge.JPとして運営されていたので、そちらのほうに馴染みがあるかたも多いことでしょう。
<!--more-->


  * [Slashdot JapanならびにSourceForge.JP、サイト名称変更のお知らせ](http://osdn.co.jp/press/2015/04/%E3%82%B5%E3%82%A4%E3%83%88%E5%90%8D%E7%A7%B0%E5%A4%89%E6%9B%B4%E3%81%AE%E3%81%8A%E7%9F%A5%E3%82%89%E3%81%9B)

OSDNへとブランド名称が変更されたのが2015年5月のことですから、新ブランドへ移行して、まもなく1年ということになりますね。
今年に入ってからのOSDNのトピックとしては、ファイルリリースのミラーの増強が行われたようです。そして、2016年2月からはAPIの提供がはじまりました。

  * [API (β)の提供を開始します](https://osdn.jp/projects/sourceforge/news/25232)

今回は提供されているAPIのうち、リリースに関連した部分を以下の流れで紹介します。

  * OSDNのファイルリリース機能について

  * APIとクライアントライブラリについて

  * osdn-cliとは

  * クライアントライブラリの使い方

### OSDNのファイルリリース機能について

OSDNでのリリースは、[ファイルリリースガイド](https://osdn.jp/docs/FileRelease_Guide)にあるように基本的に3つの要素から構成されています。

  * パッケージ（デフォルトではプロジェクト名と同じ）

  * リリース（各パッケージにひもづけて作成する。複数リリース可能）

  * ファイル（各リリースにひもづけて作成する。複数アップロード可能）

ただし、リリースの下に追加できるのは、ファイルのみという制約があります。ディレクトリを追加するようなことはできません。

類似のサービスであるSourceForge.netと大きく違うのがこの点です。SourceForge.netでは、ファイルリリースのために、frs.sourceforge.netというサイトが用意されています。
frs.sourceforge.netでは、所定のディレクトリへとアップロードしたディレクトリツリーはそのまま公開されるようになっています。
OSDNのような制約がないので、rsyncでアップロードできます。

上記制約で問題にならないケースがほとんどだと思われますが、例外もあります。
それはプロジェクトがパッケージのリポジトリを提供しようとしている場合です。
aptやyumを使って簡単にインストールできるようにdebパッケージやrpmパッケージのリポジトリを提供しようとすると、リポジトリのディレクトリツリーがOSDNのこの制約にひっかかります。

プロジェクトWeb[^0]配下に置くという裏技がありますが、これはネットワークの帯域的な問題およびファイルシステムリソース的な問題を起こしかねないので、やってはいけないことになっています。
OSDN側で問題は認識しているものの、下記のチケットをみる限り残念ながらこの制約に関して進展はなさそうです。[^1]

  * [シェルサーバからリリース公開する手段](https://osdn.jp/ticket/browse.php?group_id=1&tid=35773)

### APIとクライアントライブラリについて

どんなAPIが提供されているかは、[APIエクスプローラ](https://osdn.jp/swagger-ui/)にて確認できます。

このAPIの提供に合わせて、それらを簡単に扱えるようなクライアントライブラリ`osdn-client`もリリースされています。
この記事を書いている4月時点で、Ruby/PHP/Pythonの3種類が提供されています。
OSDN Codeから各種言語向けのアーカイブをダウンロードできます。

  * [OSDN Code](https://osdn.jp/projects/osdn-codes/releases/)

クライアントライブラリの使い方については後述します。

### osdn-cliとは

前述のクライアントライブラリ`osdn-client`を利用したコマンドラインツールです。
APIの使用例として参考になることでしょう。Rubyで実装されています。

  * [osdn-cli](https://osdn.jp/projects/osdn-codes/scm/git/osdn-cli/)

以下のようにして、gemとしてインストールすることができます。

```
% gem install osdn-cli
```


インストールが完了すると、`osdn`コマンドが使えるようになります。
`osdn login`を実行すると、ブラウザでアプリケーション連携確認画面が表示されます。

![アプリケーション連携確認]({{ "/images/blog/20160405_0.jpg" | relative_url }} "アプリケーション連携確認")

許可すると、認証コードが表示されるので、ターミナルにて、認証コードを入力します。

```
% osdn login
Access following URL to get auth code;

Type your auth code: (ここで認証コードを入力)
```


クレデンシャル情報については、`~/.config/osdn/credential.yml`として保存されるようになっています。

```
% cat ~/.config/osdn/credential.yml
---
access_token: (ここにアクセストークン)
expires_in: 86400
token_type: Bearer
scope:
- profile
- group
- group_write
refresh_token: (ここにリフレッシュトークン)
expires_at: 2016-04-02 15:56:29.336425089 +09:00
```


このあと説明するクライアントライブラリでは、上記のアクセストークンを使います。

### クライアントライブラリの使い方

クライアントライブラリの使い方を次の3つの観点から紹介します。

  * アクセストークンの設定（共通）

  * 参照系のAPIの使い方

  * 更新系のAPIの使い方

#### アクセストークンの設定

アクセストークンの設定は次のようにして行います。アクセストークンの値については、`credential.yml`を参照してください。

```ruby
OSDNClient.configure do |config|
  config.access_token = "(ここにアクセストークン)"
end
```


#### 参照系のAPIの使い方

次の4点について説明します。

  * プロジェクト情報の取得

  * パッケージ情報の取得

  * リリース情報の取得

  * ファイル情報の取得

##### プロジェクト情報の取得

プロジェクト情報の取得には、`get_project`を使います。

```ruby
api = OSDNClient::ProjectApi.new
proj_info = api.get_project("プロジェクト名")
p proj_info
```


##### パッケージ情報の取得

パッケージ情報の取得には、`list_packages`を使います。

```ruby
api = OSDNClient::ProjectApi.new
packages = api.list_packages("プロジェクト名")
packages.each do |package|
  p package
end
```


後述するファイル情報取得では、パッケージ固有のIDを`package.id`として引数に渡します。

##### リリース情報の取得

リリース情報の取得には、各パッケージの`releases`を参照します。

```ruby
api = OSDNClient::ProjectApi.new
packages = api.list_packages("プロジェクト名")
packages.each do |package|
  package.releases.each do |release|
    p release
  end
end
```


後述するファイル情報取得では、リリース固有のIDを`release.id`として引数に渡します。

##### ファイル情報の取得

ファイル情報の取得には、`get_release`を使い、リリース情報の`files`を参照します。

```ruby
api = OSDNClient::ProjectApi.new
packages = api.list_packages("プロジェクト名")
packages.each do |package|
  package.releases.each do |release|
    release_info = api.get_release("プロジェクト名", package.id, release.id)
      p release_info.files
    end
  end
end
```


#### 更新系のAPIの使い方

次の4点について説明します。

  * パッケージの作成

  * リリースの作成

  * ファイルの作成

  * ニュースの作成

#### パッケージの作成

パッケージの作成には、`create_package`を使います。

```ruby
package = api.create_package("プロジェクト名", "パッケージ名")
```


#### リリースの作成

リリースの作成には、`create_release`を使います。

```ruby
package = api.create_package("プロジェクト名", "パッケージ名")
release = api.create_release("プロジェクト名", package.id, "リリース名")
```


リリースはパッケージにひもづいているので、パッケージ固有のIDを`package.id`として引数に渡します。

#### ファイルの作成

ファイルの作成には、`create_release_file`を使います。

```ruby
package = api.create_package("プロジェクト名", "パッケージ名")
release = api.create_release("プロジェクト名", package.id, "リリース名")
open("ファイル") do |file|
  file_info = api.create_release_file("プロジェクト名", package.id, release.id, file)
end
```


ファイルはパッケージとリリースにひもづいているので、パッケージ固有のIDを`package.id`、リリース固有の値を`release.id`として引数に渡します。

#### ニュースの作成

ニュースの作成は、`create_news_0`を使います。`create_news`ではないことに注意してください。

```ruby
api.create_news_0("プロジェクト名", title, body)
```


ニュースは、パッケージやリリースとは独立しているのでタイトルを`title`、本文を`body`として引数に渡します。

### まとめ

今回は、OSDNのファイルリリース機能をAPI経由で使う方法をパッケージ/リリース/ファイルの参照と作成に焦点をあてて紹介しました。
もし、リリース時のファイルのアップロードを自動化したいというときには、APIの利用を検討してみるといいかもしれません。

[^0]: プロジェクトWebとはhttps://(プロジェクト名).osdn.jp/としてアクセスされるもののこと

[^1]: .htaccessでrewriteすることで、ファイルリリースシステム側へマッピングするという手も試してみたのですが、残念ながらエラーになるため使えませんでした。
