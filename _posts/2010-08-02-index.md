---
tags: []
title: サーバ上でPDFやオフィス文書からテキストを抜き出す方法あれこれ
---
[groonga](http://groonga.org/)などを使って全文検索システムを作るときは、PDFやオフィス文書などからテキスト情報を抜きだして検索用インデックスを作る必要があります。Windowsでテキストを抽出するソフトウェアとしては[xdoc2txt](http://www31.ocn.ne.jp/~h_ishida/xdoc2txt.html)などがありますが、ここでは、Linuxサーバ上でテキストを抽出する方法を紹介します。
<!--more-->


### PDF

Linux上でPDFを閲覧する場合は、昔は[Xpdf](http://www.foolabs.com/xpdf/)でしたが、最近は[Evince](https://ja.wikipedia.org/wiki/Evince)や[Okular](http://okular.kde.org/)の方がよく使われているようです。どちらもPDFの処理にはXpdfからforkした[Poppler](http://poppler.freedesktop.org/)というライブラリを使っています。

popplerにはPDFからテキストを抽出するpdftotextというコマンドが付属しているため、それを利用してPDFからテキストを抽出できます。

{% raw %}
```
% pdftotext hello.pdf hello.txt
```
{% endraw %}

これでhello.pdfのテキスト情報がhello.txtに出力されます。

### Word文書・OpenDocumentFormatテキスト文書

WordやOpenOffice.org Writerで作成した文書は[AbiWord](http://www.abisource.com/)でテキスト情報を抽出できます。

AbiWordは通常はGUI付きで起動しますが、オプションを指定することによりフィルタコマンドとしても利用できます。

Word文書からテキストを抽出するコマンドは以下の通りです。

{% raw %}
```
% abiword --to txt --to-name hello.txt hello.doc
```
{% endraw %}

OpenDocumentFormatテキスト文書からテキストを抽出する場合も同じオプションです。

{% raw %}
```
% abiword --to txt --to-name hello.txt hello.odt
```
{% endraw %}

### Excelスプレッドシート・OpenDocumentFormatスプレッドシート

ExcelやOpenOffice.org Calcで作成したスプレッドシートは[Gnumeric](https://ja.wikipedia.org/wiki/Gnumeric)に付属するssconvertでCSVに変換できます。

ExcelスプレッドシートをCSVに変換するコマンドは以下の通りです。

{% raw %}
```
% ssconvert --export-type Gnumeric_stf:stf_csv hello.xls hello.csv
```
{% endraw %}

OpenDocumentFormatスプレッドシートをCSVに変換する場合も同じオプションです。

{% raw %}
```
% ssconvert --export-type Gnumeric_stf:stf_csv hello.ods hello.csv
```
{% endraw %}

### PowerPointスライド・OpenDocumentFormatプレゼンテーション

PowerPointやOpenOffice.org Impressで作成したスライドをテキストに変換する方法はいくつかあるのですが、OpenOffice.orgを使う方法を紹介します。OpenOffice.orgを使う方法はWordやExcelの時も使えますが、事前に準備が必要なので、多少面倒です。

OpenOffice.orgを使って変換する方法にはOpenOffice.orgを変換サーバとして使う方法とコマンドとして使う方法がありますが、ここではコマンドとして使う方法を紹介します。コマンドとして使う場合もいろいろやり方がありますが、今回は、まずPDFに変換し、PDFからは上述のPopplerを使って変換することにします。

まず、以下の内容の~/.openoffice.org/3/user/basic/Standard/Export.xbaを作ります。

{% raw %}
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE script:module PUBLIC "-//OpenOffice.org//DTD OfficeDocument 1.0//EN" "module.dtd">
<script:module xmlns:script="http://openoffice.org/2000/script" script:name="Export" script:language="StarBasic">
sub WritePDF(url as string)
dim document   as object
dim dispatcher as object
document   = ThisComponent.CurrentController.Frame
dispatcher = createUnoService(&quot;com.sun.star.frame.DispatchHelper&quot;)

dim args1(1) as new com.sun.star.beans.PropertyValue
args1(0).Name = &quot;URL&quot;
args1(0).Value = url
args1(1).Name = &quot;FilterName&quot;
args1(1).Value = &quot;writer_pdf_Export&quot;

dispatcher.executeDispatch(document, &quot;.uno:ExportDirectToPDF&quot;, &quot;&quot;, 0, args1())

document.close(true)

end sub
</script:module>
```
{% endraw %}

そして、同じディレクトリにある~/.openoffice.org/3/user/basic/Standard/script.xlbに登録して、以下のような内容にします。<library:element>のところを追加しています。

{% raw %}
```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE library:library PUBLIC "-//OpenOffice.org//DTD OfficeDocument 1.0//EN" "library.dtd">
<library:library xmlns:library="http://openoffice.org/2000/library" library:name="Standard" library:readonly="false" library:passwordprotected="false">
 <library:element library:name="Export"/>
</library:library>
```
{% endraw %}

これで準備ができました。以下のようにスライドをPDFに変換できます。

{% raw %}
```
% ooffice -headless hello.ppt 'macro:///Standard.Export.WritePDF("file:///tmp/hello.pdf")'
```
{% endraw %}

PowerPointのファイル以外でもWord文書などOpenOffice.orgで開けるファイルを指定すれば、それらもPDFに変換できます。

以前はXがない状態では動かないため、Xvfbなど仮想的なXサーバを使ったものですが、最近のOpenOffice.orgはXがなくても動作するようです。すばらしい。

PDFに変換したら後は上述の方法でテキストを抽出できますね。

### まとめ

Linuxサーバ上でPDFやオフィス文書からテキストを抽出する方法を紹介しました。以前はwvTextやxlhtml、ppthtmlなどを使うことが多かったでしょうが、最近はもっと精度よくテキストを抽出できるようになっているので、ここで紹介したツールも試してみてはいかがでしょうか。
