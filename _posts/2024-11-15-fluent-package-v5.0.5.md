---
title: LTS版 Fluent Package v5.0.5をリリース
author: kenhys
tags:
  - fluentd
---

2024年11月14日にFluent PackageのLTSの最新版となるv5.0.5をリリースしました。

本リリースでは、パッケージでいくつかの修正が行われています。

本記事ではFluent Packageの変更内容について紹介します。

<!--more-->

## Fluent Package v5.0.5

2024年11月14日にFluent PackageのLTSの最新版となるv5.0.5をリリースしました。
同梱のRubyは3.2.6へ、Fluentdのバージョンはv1.16.5からv1.16.6に更新しています。

本記事ではFluent Packageの変更内容について紹介します。
興味のある方は、以下の公式情報もご覧ください。

公式リリースアナウンス

* Fluent Package v5.0.5: https://www.fluentd.org/blog/fluent-package-v5.0.5-has-been-released

CHANGELOG

* Fluent Package v5.0.5: https://github.com/fluent/fluent-package-builder/releases/tag/v5.0.5

1.16.6の修正内容については、公式アナウンスである[Fluentd v1.16.6 has been released](https://www.fluentd.org/blog/fluentd-v1.16.6-has-been-released)を参考にしてください。

## 修正

Fluent Package v5.0.5では、以下の修正をしています。

* Fluentdのプロセスがメモリ不足で終了することがある問題を修正
* WindowsでGEM_HOME/GEM_PATHが上書きされてしまう不具合を修正

### Fluentdのプロセスがメモリ不足で終了することがある問題を修正

細工したメッセージをFluentdに送りつけたときに、Fluentdのプロセスがメモリ不足で終了することがある問題を修正しました。

そのようなメッセージを受け取ったとき、Fluentdが依存しているmsgpackライブラリーが事前にメモリを多めに確保しようとして失敗することがあるためです。
msgpack 1.7.3では事前に確保するメモリを一定量までに制限する修正がなされたので、そのような細工されたメッセージをFluentdが受け取っても
問題なく動作するようになりました。

外部から特段の制限なくデータを受け付けるようにしていなければ影響ありません。

### WindowsでGEM_HOME/GEM_PATHが上書きされてしまう不具合を修正

管理者権限でFluent Package Promptを起動し、`GEM_HOME`や`GEM_PATH`を書き換えることで
意図しないRubyの依存ライブラリーを利用できてしまう不具合を修正しました。

Fluent Package Promptから明示的にFluentdを起動しようとした場合に影響を受けていました。

FluentdをWindowsのサービスとして動作させる場合には影響ありません。

## その他の修正・改善

* Rubyのバージョンを3.2.6にアップデート

  * Rubyのバージョンを3.2.4から3.2.6に更新しています。したがって、Ruby 3.2.5およびRuby 3.2.6のセキュリティアップデートが含まれます。
  * Ruby 3.2.5のセキュリティアップデートに関する詳細は[Ruby 3.2.5 リリース](https://www.ruby-lang.org/ja/news/2024/07/26/ruby-3-2-5-released/)をご覧ください。
  * Ruby 3.2.6のセキュリティアップデートに関する詳細は[Ruby 3.2.6 リリース](https://www.ruby-lang.org/ja/news/2024/10/30/ruby-3-2-6-released/)をご覧ください。

## まとめ

今回は、Fluentdの最新パッケージFluent Package v5.0.5の情報をお届けしました。

パッケージでいくつかの修正が行われました。
最新版のFluent Packageへのアップデートを推奨します。

長期の安定運用がしやすいLTS版をぜひご活用ください。

* [Download Fluent Package](https://www.fluentd.org/download/fluent_package)
* [Installation Guide](https://docs.fluentd.org/installation)

また、クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
Fluent Packageへのアップデートについてもサポートいたしますので、詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。

最新版を使ってみて何か気になる点があれば、ぜひ[GitHub](https://github.com/fluent/fluent-package-builder/issues)でコミュニティーにフィードバックをお寄せください。
また、[日本語用Q&A](https://github.com/fluent/fluentd/discussions/categories/q-a-japanese)も用意しておりますので、困ったことがあればぜひ質問をお寄せください。
