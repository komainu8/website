---
title: 通常版Fluent Package v5.1.0をリリース
author: fujita
tags:
  - fluentd
---
こんにちは。[Fluentd](http://www.fluentd.org)チームの藤田です。

2024年8月2日に通常版Fluent Package v5.1.0をリリースしました。
バグ修正やセキュリティ対応を主とするLTS版とは異なり、通常版は新機能などもリリースに含まれます。

本リリースでは、パッケージでいくつかの修正と改善を行い、さらにFluentdをv1.17.0に更新しました。

本記事ではFluent Packageの変更内容について紹介します。

<!--more-->

## Fluent Package v5.1.0

2024年8月2日に通常版Fluent Package v5.1.0をリリースしました。

本記事ではFluent Packageの変更内容について紹介します。
興味のある方は、以下の公式情報もご覧ください。

公式リリースアナウンス

* Fluentd v1.17.0: https://www.fluentd.org/blog/fluentd-v1.17.0-has-been-released
* Fluent Package v5.1.0: https://www.fluentd.org/blog/fluent-package-v5.1.0-has-been-released

CHANGELOG

* Fluentd v1.17.0: https://github.com/fluent/fluentd/releases/tag/v1.17.0
* Fluent Package v5.1.0: https://github.com/fluent/fluent-package-builder/releases/tag/v5.1.0

### 改善

通常版Fluent Package v5.1.0では、以下の改善を行いました。

* 同梱のFluentdをv1.17.0に更新
* Rubyのバージョンを3.2.5にアップデート
  * Ruby 3.2.5には多数のバグ修正が含まれています。詳細は https://github.com/ruby/ruby/releases/tag/v3_2_5 をご覧ください。
* 同梱のRubyライブラリのバージョン更新

Fluentd v1.17.0の更新内容については以降で紹介します。

## Fluentd v1.17.0

### 新機能

Fluentd v1.17.0では、以下の新機能を追加しました。

* in_tailプラグイン: 対象ファイルのパターン指定のためにglob_policyオプションを追加
* out_httpプラグイン: AWS Signature Version 4のサポートを追加
* out_httpプラグイン: コネクションを再利用するためのオプションを追加
* in_httpプラグイン: Content Security Policy (CSP)のレポートをJSONとして処理

#### in_tailプラグイン: 対象ファイルのパターン指定のためにglob_policyオプションを追加

これまでは`path`や`exclude_path`で`*`のみがワイルドカードとして使用することができました。

v1.17.0からは`glob_policy extended`を指定することで、`[]{}?`を使用しパスにマッチさせるパターンを指定できます。

例えば以下のように設定すると、`02.log`、`03.log`、`12.log`、`13.log`のみにマッチさせるというような書き方ができるようになります。

```
path "[0-1][2-3].log"
glob_policy extended
```

`glob_policy`を設定しなかった場合は従来どおりの挙動となり、上記設定では`[0-1][2-3].log`というファイルにマッチします。

#### out_httpプラグイン: AWS Signature Version 4のサポートを追加

BASIC認証に加え、v1.17.0からは`auth`セクションの`method`で`aws_sigv4`を指定することで[AWS Signature Version 4](https://docs.aws.amazon.com/AmazonS3/latest/API/sig-v4-authenticating-requests.html)を認証方式として使用できます。

例として、以下のような設定を行うことでAWS Signature Version 4を使用できます。

```
<auth>
  method aws_sigv4
  aws_service osis
  aws_region us-east-1
  aws_role_arn arn:aws:iam::123456789012:role/MyRole
</auth>
```

#### out_httpプラグイン: コネクションを再利用するためのオプションを追加

これまではHTTPリクエストのたびにコネクションを作成して通信していました。そのため、TLSの利用時や遅延が大きいネットワークでは
パフォーマンスが低下する傾向がありました。

v1.17.0では`reuse_connections`オプションが追加され、これを有効にするとHTTPコネクションが再利用されるようになり、
パフォーマンスの向上が期待されます。

#### in_httpプラグイン: Content Security Policy (CSP)のレポートをJSONとして処理

`Content-Type`が`application/csp-report`であるリクエストのデータタイプを、デフォルトでJSONとして扱うようになりました。
これにより、[Content Security Policy](https://developer.mozilla.org/en-US/docs/Web/HTTP/CSP)のレポートをデフォルトで扱えるようになりました。

### 修正

Fluentd v1.17.0では、以下の不具合修正を行いました。

* 一部のParserプラグインが意図とは異なるデータを返却することがある問題を修正

#### 一部のParserプラグインが意図とは異なるデータを返却することがある問題を修正

Parserプラグインでは`Hash`型のデータを返却する必要があるのですが、流入するデータによっては以下のプラグインで意図通りのデータを返却していませんでした。

* parser_json
* parser_msgpack

これまでは設定次第でエラーが発生することがあり、今回のリリースで修正しました。

また、この修正によりfilter_parserプラグインに制限が生じます。
配列データを含む文字列をパースすると、結果として複数のデータが取得されることがありえますが、
filter_parserは1つ目のデータしか処理できません。

処理できなかったデータは、エラーイベント(`@ERROR`ラベルのイベント)として処理されます。
`@ERROR`ラベルを設定していない場合は、次の警告ログによって処理できなかったデータを確認できます。

```
{datetime} [warn]: #0 dump an error event: error_class=Fluent::Plugin::Parser::ParserError error="Could not emit the event. The parser returned multiple results, but currently filter_parser plugin only returns the first parsed result. Raw data: ..." location=nil tag="..." time=... record=...
```

この制限については、今後のバージョンで改善する予定です。

#### エラーの例

以前のバージョンで、以下のような設定を行います。

```
<source>
  @type tcp
  tag test.tcp
  <parse>
    @type json
    null_empty_string
  </parse>
</source>

<match test.**>
  @type stdout
</match>
```

`in_tcp`に対してJSON文字列で配列のデータを送信します。

```
$ netcat 0.0.0.0 5170
[{"k":"v"}, {"k2":"v2"}]
```

以前のバージョンでは、以下のようなエラーが発生しました。

```
{datetime} [error]: #0 unexpected error on reading data host="xxx" port=xxx error_class=NoMethodError error="undefined method `each_key' for [{\"k\"=>\"v\"}, {\"k2\"=>\"v2\"}]:Array"
```

v1.17.0からは以下のように出力されます。

```
{datetime} test.tcp: {"k":"v"}
{datetime} test.tcp: {"k2":"v2"}
```

## まとめ

今回は、通常版Fluent Package v5.1.0のリリース情報をお届けしました。

本リリースでは、パッケージでいくつかの修正と改善を行い、Fluentdをv1.17.0に更新しました。
Fluentd v1.17.0では、挙動の変更を伴う修正も行いました。

長期の安定運用がしやすいLTS版もぜひご活用ください。

* [Download Fluent Package](https://www.fluentd.org/download/fluent_package)
* [Installation Guide](https://docs.fluentd.org/installation)

また、クリアコードは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})を行っています。
Fluent Packageへのアップデートについてもサポートいたしますので、詳しくは[Fluentdのサポートサービス]({% link services/fluentd-service.md %})をご覧いただき、[お問い合わせフォーム]({% link contact/index.md %})よりお気軽にお問い合わせください。

最新版を使ってみて何か気になる点があれば、ぜひ[GitHub](https://github.com/fluent/fluentd/issues)でコミュニティーにフィードバックをお寄せください。
また、[日本語用Q&A](https://github.com/fluent/fluentd/discussions/categories/q-a-japanese)も用意しておりますので、困ったことがあればぜひ質問をお寄せください。
