---
tags: []
title: Firefoxのアップデートのしくみについて
---
### はじめに

Windows版のFirefoxでは、メニューから「Firefoxについて」を選択すると、最新のバージョンのチェックを行えます。
この際、更新があればアップデートを適用できるようになっています。[^0]
<!--more-->


今回は、Firefoxのアップデートのしくみがどのようになっているのかを以下の技術資料を参考に紹介します。

  * [MDN > Mozilla > 更新サーバの設定](https://developer.mozilla.org/ja/docs/Mozilla/Setting_up_an_update_server#.E3.82.AF.E3.83.A9.E3.82.A4.E3.82.A2.E3.83.B3.E3.83.88.E3.82.B5.E3.82.A4.E3.83.89.E3.81.AE.E8.A8.AD.E5.AE.9A)

### Firefoxアップデートの流れ

Firefoxの更新がある場合、以下の流れで適用されます。（手動で明示的に更新する場合）

  * 「Firefoxについて」をメニューからクリック

  * Firefoxが更新サーバにバージョンチェックのリクエストを出す

  * 更新サーバがバージョンチェック結果をレスポンスとして返す

  * Firefoxはレスポンスを確認して、アップデートがあればアップデートファイルを更新サーバにリクエストする（なければなにもしない）

  * 更新サーバがアップデートファイルを返す

  * Firefoxはアップデートファイルをダウンロードし、ダイアログに「Firefoxを再起動して更新」ボタンを表示する

  * ユーザーがボタンをクリックするとFirefoxを再起動、更新を適用する

ここでポイントとなるのがバージョンチェックとアップデートファイルのダウンロードの部分です。
それぞれもう少し詳しく説明します。

#### バージョンチェックのリクエスト

バージョンチェックでは、実際にどのようなリクエストを更新サーバに出しているのでしょうか。実際に確認してみましょう。
これはURL入力欄に `about:config` とタイプして設定画面を表示させ、 `app.update.url` を検索してみるとわかります。
`app.update.url` はバージョンチェックするためにアクセスするURLです。

45.6.0ESRの場合は以下のとおりです。[^1]

```text
https://aus5.mozilla.org/update/6/%PRODUCT%/%VERSION%/%BUILD_ID%/%BUILD_TARGET%/%LOCALE%/%CHANNEL%/%OS_VERSION%(nowebsense)/%SYSTEM_CAPABILITIES%/%DISTRIBUTION%/%DISTRIBUTION_VERSION%/update.xml
```


いくつか `%XXX%` というパラメータがあります。これはリクエストするときに実際の値に置換されます。それぞれの意味は次の通りです。

  * `%PRODUCT%` プロダクト名です。「Firefox」となります。

  * `%VERSION%` 45.6.0ESRの場合「45.6.0」です。

  * `%BUILD_ID%` 45.6.0ESRの場合は「20161209150850」です。

  * `%BUILD_TARGET%` 32bit版のFirefoxでは「WINNT_x86-msvc-x64」です。64bit版だと「WINNT_x86_64-msvc-x64」です。

  * `%LOCALE%` 日本語版なので「ja」です。

  * `%CHANNEL%` どのエディションなのかを示す値です。ESR版なので「esr」です。

  * `%OS_VERSION%` 使用しているOSを示す値です。Windows 7 64bit版だと「Windows_NT%206.1.1.0%20(x64)」です。

  * `%SYSTEM_CAPABILITIES%` 環境によっておそらく異なる値となります。手元の環境では「SSE3」でした。

  * `%DISTRIBUTION%` Firefox公式なので「default」です。

  * `%DISTRIBUTION_VERSION%` Firefox公式なので「default」です。

したがって、実際のリクエストは以下のようになります。

```text
https://aus5.mozilla.org/update/6/Firefox/45.6.0/20161209150850/WINNT_x86-msvc-x64/ja/esr/Windows_NT%206.1.1.0%20(x64)(nowebsense)/SSE3/default/default/update.xml
```


このとき実際に更新サーバから返ってくるレスポンスは以下のとおりです。

```xml
<?xml version="1.0"?>
<updates>
    <update type="minor" displayVersion="45.7.0esr" appVersion="45.7.0" platformVersion="45.7.0" buildID="20170118123525" detailsURL="https://www.mozilla.org/ja/firefox/45.7.0/releasenotes/">
        <patch type="complete" URL="http://download.mozilla.org/?product=firefox-45.7.0esr-complete&amp;os=win&amp;lang=ja" hashFunction="sha512" hashValue="6168bcaa9424fe1d789c80cf54fe55c8b4e70f2d5468ebbeaef1f2776a3a840806b1ac270306852684a45ff9ad050de3f46a149dc2747d4a6b9440e0c27bf8a5" size="52388819"/>
        <patch type="partial" URL="http://download.mozilla.org/?product=firefox-45.7.0esr-partial-45.6.0esr&amp;os=win&amp;lang=ja" hashFunction="sha512" hashValue="885218494c4b2b8ecd9c1c696b84f5850e78898cd289e28aa6cd8b6f727fa4dbd363de3edb6da861456ab01bb2c38639fbdc93f01746e89e7de4c3d836fceab5" size="6470506"/>
    </update>
</updates>
```


更新対象として、45.7.0ESRが存在していることがわかります。[^2]

次のようにコマンドラインからパラメータを明示してバージョンチェックのリクエストを投げることもできます。[^3]

```text
curl "https://aus5.mozilla.org/update/6/Firefox/45.6.0/20161209150850/WINNT_x86-msvc-x64/ja/esr/Windows_NT%206.1.1.0%20(x64)(nowebsense)/SSE3/default/default/update.xml"
```


#### アップデートファイルのダウンロード

レスポンスの例で示したように、更新サーバはアップデートファイルへのリンクを返します。アップデートがなければ以下のように空のタグを返します。

```xml
<?xml version="1.0"?>
<updates>
</updates>
```


アップデートがあるときには差分版もしくは完全版がダウンロードされますが、これらはどこから入手したらよいのでしょうか。

これには、CDN経由でダウンロードするやりかたがあります。以下のような所定のディレクトリ階層でアップデートファイルが提供されています。

```text
http://download.cdn.mozilla.net/pub/firefox/releases/(ESRのバージョン)/update/(32bit or 64bit)/(言語)/
```


したがって45.7.0ESRの場合、45.6.0ESRから45.7.0ESRへの差分版と45.7.0ESRへの完全版のアップデートファイルが以下から入手できます。

```text
http://download.cdn.mozilla.net/pub/firefox/releases/45.7.0esr/update/win32/ja/
```


このディレクトリには以下の3つのファイルがあります。

  * firefox-45.6.0esr-45.7.0esr.partial.mar

  * firefox-45.6.0esr-45.7.0esr.partial.mar.asc

  * firefox-45.7.0esr.complete.mar

このうち、アップデートファイルは拡張子が.marのものです。marとは `Mozilla ARchive` に由来しています。
marファイルのフォーマットに興味がある方は、[Software_Update:MAR](https://wiki.mozilla.org/Software_Update:MAR)を参照するとよいでしょう。

`firefox-45.6.0esr-45.7.0esr.partial.mar` は45.6.0ESRから45.7.0ESRへ差分更新する際に適用されるファイルで、 `firefox-45.7.0esr.complete.mar` は45.7.0ESRへ完全版でアップデートするときに適用されるファイルです。

### まとめ

今回は、Firefoxのアップデートのしくみについて紹介しました。
このしくみを利用すれば、企業内でFirefoxの更新サーバを独自に立てることも可能です。
そのあたりの話は別の記事にて紹介したいと考えています。

[^0]: Linuxディストリビューションの場合など、アプリケーションの管理ポリシーによっては無効になっていることがあります。

[^1]: 2017年3月時点では45.7.0ESRが最新ですが、バージョンチェックの確認のため一つ前のバージョンを例に出しています。

[^2]: partialというのは差分版、completeは完全版のことです。例えば45.6.0ESRから45.7.0ESRは1世代前なので差分更新できますが、45.5.0ESRからは2世代前なので完全版でアップデートします。

[^3]: 45.3.0未満だといったん45.3まで上げてから最新版へとアップデートされることに気がつくことでしょう。
