---
tags: []
title: Sylpheedのプラグインの作り方（2015年版）
---
### はじめに

[Sylpheed](http://sylpheed.sraoss.jp/ja/)という「軽快で使いやすいオープンソースのメールソフト」があります。
以前、Sylpheed向けにプラグインを作る方法をWindows版を前提に紹介しました。
<!--more-->


  * [Sylpheedのプラグインの作り方]({% post_url 2013-03-26-index %})

もう2年以上前の記事となり、内容の一部が若干古くなっていることから、改めてソースコードに添付されているプラグインを作る手順を紹介します。[^0]

### 対象とするSylpheedのバージョン

Sylpheedには、バージョン3.0から正式に導入されたプラグインの仕組みがあります。
このプラグインの仕組みはしばしば改訂[^1]されています。
そのため、本記事で対象とするSylpheedのバージョンは現在リリースされている最新の安定版である3.4.3 Windows版とします。[^2]
もう間もなく3.5.0が正式リリースされますが、本記事の内容はそのまま使えるはずです。

### Sylpheedのプラグインでできること

Sylpheedのプラグインの仕様は[プラグイン仕様](http://sylpheed.sraoss.jp/wiki/index.php?%A5%D7%A5%E9%A5%B0%A5%A4%A5%F3%BB%C5%CD%CD>)として文書化されています。
基本的に以下の2つのライブラリが提供するAPIを利用して、Sylpheedと連携して動作させる仕組みになっています。

  * LibSylphメールライブラリ(libsylph)

  * プラグインインターフェースライブラリ(libsylpheed-plugin)

具体的に利用できるAPI一覧については、ソースアーカイブに含まれている `libsylph/libsylph-0.def` と `src/libsylpheed-plugin-0.def` でエクスポートされている内容で知る事ができます。

LibSylphメールライブラリ（libsylph）では、ネットワークやメールに関する機能を利用するためのAPIが数多く提供されています。

プラグインインターフェースライブラリ（libsylpheed-plugin）では、プラグインからユーザインタフェースを制御するためのAPIが多数提供されています。
プレフィクスが `syl_plugin_` とついているものが該当します。

つまり、この2つのライブラリが提供しているAPIと組み合わせてできることなら、プラグインの実装次第で何でもできるというわけです。

#### プラグインから利用できるシグナル

プラグインでは、シグナルと呼ばれるイベントに対応したコールバックを登録することで、ユーザの操作に応じた処理を行なわせることができます。
プラグインから利用できるシグナルは以下の通りです。

  <table>
    <tr><th>シグナル</th><th>シグナルの発行タイミング</th></tr>
    <tr><td>folderview-menu-popup</td><td>メールの振り分けフォルダツリー表示で右クリックしたときに発行されます。</td></tr>
    <tr><td>summaryview-menu-popup</td><td>サマリビュー(件名等の一覧表示)で右クリックしたときに発行されます。</td></tr>
    <tr><td>compose-send</td><td>メール送信を実行したときに発行されます。</td></tr>
    <tr><td>compose-created</td><td>メール作成画面を表示したときに発行されます。</td></tr>
    <tr><td>compose-destroy</td><td>メール作成画面を閉じたときに発行されます。</td></tr>
    <tr><td>textview-menu-popup</td><td>メッセージビュー(メール本文を表示)で右クリックしたときに発行されます。</td></tr>
    <tr><td>messageview-show</td><td>メッセージビューにメールを表示したときに発行されます。</td></tr>
    <tr><td>inc-mail-start</td><td>メール受信開始時に発行されます。</td></tr>
    <tr><td>inc-mail-finished</td><td>メール受信終了時に発行されます。</td></tr>
    <tr><td>prefs-common-open</td><td>全般の設定画面を表示したときに発行されます。</td></tr>
    <tr><td>prefs-account-open</td><td>アカウント編集画面を表示したときに発行されます。</td></tr>
    <tr><td>prefs-filter-open</td><td>振り分けの設定画面を表示したときに発行されます。</td></tr>
    <tr><td>prefs-filter-edit-open</td><td>フィルタルール編集画面を表示したときに発行されます。</td></tr>
    <tr><td>prefs-template-open</td><td>テンプレート設定画面を表示したときに発行されます。</td></tr>
    <tr><td>plugin-manager-open</td><td>プラグイン管理画面を表示したときに発行されます。</td></tr>
  </table>


### プラグインの紹介

Sylpheedで公式に提供されているプラグインには以下の2つがあります。

  * `attachment_tool`

  * `test`

ソースパッケージsylpheed-3.4.3.tar.gzもしくはsylpheed-3.4.3.tar.bz2の `plugin/attachment_tool` ディレクトリおよび `plugin/test` ディレクトリにプラグインのソース一式が含まれています。
[^3]

公開中のSylpheedのプラグインについては、[プラグイン](http://sylpheed.sraoss.jp/ja/plugin.html)ページにいくつか有志によるプラグインが紹介されています。

#### `attachment_tool` プラグインとは

`attachment_tool` プラグインは [Sylpheedのプラグイン紹介ページ](http://sylpheed.sraoss.jp/ja/plugin.html)にて紹介されていますが、添付ファイルを削除するためのプラグインです。

任意のメールを選択した状態で、メニューから `[ツール] - [添付ファイルを削除]` を選択することで添付ファイルの削除を行うことができます。
対象がローカルフォルダである必要があるため、IMAPアカウントでは使えません。

MIMEの構造はそのまま残っているので、どんなファイルが添付されていたかという情報は失われません。
[^4]

#### `test` プラグインとは

`test` プラグインはプラグインの基本的な構造を実装したものです。`test` プラグインは開発者向けのため、Sylpheedのプラグイン紹介ページでは言及されていません。
Windows版ではdllとして提供されていないので、試してみるには自分でビルドする必要があります。

このプラグインは、プラグインを実装しようとするときに非常に参考になります。
プラグインが実装すべき関数やシグナルがシンプルにまとまっているからです。プラグインを作るときの雛形として使うことができます。

`test` プラグインの具体的な内容はソースパッケージの `PLUGIN.ja.txt` に記載されています。

  * プラグインのロード時に標準出力に"test plug-in loaded!"という文字列を出力する

  * フォルダの一覧を取得し、標準出力に表示する

  * Sylpheed のバージョン文字列を取得し、標準出力に表示する

  * メインウィンドウを取得し、前面に出す

  * フォルダビューの下にサブウィジェットを追加する(Testというボタンが配置されます)

  * 「ツール」メニューに「Plugin test」メニュー項目を追加する

  * 「Plugin test」メニューを選択すると、「Click this button」というボタンのみのウィンドウを表示し、クリックするとメッセージを出力する

  * アプリケーション初期化、終了、フォルダビューのコンテキストメニューポップアップ、メッセージ作成ウィンドウ作成、メッセージ作成ウィンドウ破棄のイベントを捕捉してメッセージを表示する

  * テキストビューのコンテキストメニュー表示イベントを捕捉してメニュー項目を追加する

### testプラグインを作る

では、Windowsでこのtestプラグインを作ってみましょう。
まずは、Sylpheedのプラグインをビルドするための環境構築をします。

環境構築手順については[Sylpheed Win32](http://sylpheed.sraoss.jp/wiki/index.php?Sylpheed%2FWin32)を元に記述しています。

  1. MinGWをインストールする

  1. msys-wgetをインストールする

  1. msys-unzipをインストールする

  1. mingw32-pexportsをインストールする

  1. GTK+ Windowsバイナリ（SDK）をインストールする

  1. インポートライブラリを作成する

  1. ソースコードをダウンロード/展開する

  1. 環境変数を設定する

ここまでできると、公式で配布されているインストーラ版もしくはzipアーカイブ版で使えるプラグインが作れます。

#### MinGWをインストールする

SylpheedのプラグインをビルドするためのツールはMinGWのものを利用します。ここではネットワーク経由でのインストールを行うことのできるインストーラー[mingw-get-setup.exe](http://sourceforge.net/projects/mingw/files/Installer/mingw-get-setup.exe/download)を使ってインストールします。

インストールの途中にインストールするコンポーネントを選択する箇所があります。そこでは以下を選択します。

  * mingw-developer-toolkit

  * mingw32-base

  * msys-base

以降の説明では `c:\MinGW` へインストールしたものとして説明します。

正しくインストールできていれば、`c:\MinGW\msys\1.0\msys.bat` がインストールされています。`msys.bat` を実行するとウィンドウが表示されます。
これ以降の説明では、`msys.bat` を実行して起動したシェルでの操作はプロンプトに$をつけて説明します。

コマンドラインで作業できるように、パスを通しておきましょう。

```text
$ export PATH=/c/MinGW/bin:/bin:$PATH
```


#### msys-wgetをインストールする

パッケージのダウンロードを行うのに必要なので、msys-wgetパッケージをインストールします。

```text
$ mingw-get install msys-wget
```


正しくインストールできていれば、wget --versionを実行するとバージョンが表示されます。

```text
$ wget --version
GNU Wget 1.12 built on msys.
...以下省略...
```


#### msys-unzipをインストールする

後でzipアーカイブの展開に使うのでunzipコマンドをインストールします。
インストールには以下のコマンドを実行します。

```text
$ mingw-get install msys-unzip
```


正しくインストールできていれば、unzipを引数なしで実行するとヘルプが表示されます。

```text
$ unzip
UnZip 6.00 of 20 April 2009, by Cygwin. Original by Info-ZIP.
..以下省略...
```


#### mingw32-pexportsをインストールする

後述するインポートライブラリの作成で使うので、mingw32-pexportsパッケージをインストールします。

```text
$ mingw-get install mingw32-pexports
```


正しくインストールできていれば、pexportsを引数なしで実行するとヘルプが表示されます。

```text
$ pexports
PExports 0.44 Copyright 1988, Anders Norlander
...以下省略...
```


#### GTK+ Windowsバイナリ（SDK）をインストールする

GTK+に関しては、Sylpheed向けにSDKが用意されているので、それを使います。
以降のシェルでの作業は$HOME/sylpheedディレクトリを作業ディレクトリとします。

```text
$ mkdir -p sylpheed
$ cd sylpheed
$ wget 'http://sylpheed.sraoss.jp/sylpheed/win32/MinGW/GTK-MinGW-SDK_20140603.zip'
$ unzip GTK-MinGW-SDK_20140603.zip
```


シェル上で以下のコマンドを実行します。GTK+のデモアプリケーションが起動したら正常にインストールできています。

```text
$ $HOME/sylpheed/GTK/bin/gtk-demo.exe
```


以前の記事を書いていた当時には、SDKがありませんでした。
GTK+をインストールした後に、ライブラリの個別更新が必要だったので、楽になりました。

#### インポートライブラリを作成する

ここまでで、GTK+の環境が用意できました。
プラグインのビルドには、冒頭で説明した以下の2つのライブラリが必要です。

  * LibSylphメールライブラリ（libsylph）

  * プラグインインターフェースライブラリ（libsylpheed-plugin）

Sylpheedをソースからビルドするというのも良いですが、MinGWだとビルドには時間もかかります。[^5]

そこで、Sylpheed公式サイトで配布されているzipアーカイブ版を利用してこれらライブラリのインポートライブラリを作成し、シンボルの解決はそのインポートライブラリをリンクすることによって行います。
[^6]

```text
$ cd $HOME
$ wget 'http://sylpheed.sraoss.jp/sylpheed/win32/sylpheed-3.4.3-win32.zip'
$ unzip sylpheed-3.4.3-win32.zip
$ cd Sylpheed-3.4.3
$ pexports libsylph-0-1.dll > libsylph-0-1.def
$ dlltool --dllname libsylph-0-1.dll --input-def libsylph-0-1.def --output-lib libsylph-0-1.a
$ pexports libsylpheed-plugin-0-1.dll > libsylpheed-plugin-0-1.def
$ dlltool --dllname libsylpheed-plugin-0-1.dll --input-def libsylpheed-plugin-0-1.def --output-lib libsylpheed-plugin-0-1.a
```


上記コマンドを実行して、インポートライブラリが2つ作成できていることを確認します。

  * libsylph-0-1.a

  * libsylpheed-plugin-0-1.a

#### ソースパッケージをダウンロード/展開する

プラグインのビルドに必要なヘッダが含まれているソースパッケージをダウンロード/展開します。

```text
$ cd $HOME/sylpheed
$ wget 'http://sylpheed.sraoss.jp/sylpheed/v3.4/sylpheed-3.4.3.tar.gz'
$ tar xvf sylpheed-3.4.3.tar.gz
```


#### 環境変数を設定する

GTK+関連で必要な環境変数の設定を行います。

```text
$ export PATH=$PATH:$HOME/sylpheed/GTK/bin
$ export PKG_CONFIG_PATH=$HOME/sylpheed/GTK/lib/pkgconfig
```


pkg-configコマンドが実行できることを確認します。[^7]

```text
$ pkg-config --version
0.20
```


以上でプラグインのビルドに必要なすべての環境が整いました。

#### testプラグインをビルドする

次に、展開したソースコードのtestプラグインのディレクトリへと移動し、以下のようにしてプラグインをビルドします。

```text
$ cd $HOME/sylpheed/sylpheed-3.4.3/plugin/test
$ gcc -shared -o test.dll test.c -I../../libsylph -I../../src `pkg-config --cflags gtk+-2.0` `pkg-config --libs gtk+-2.0` $HOME/Sylpheed-3.4.3/libsylph-0-1.a $HOME/Sylpheed-3.4.3/libsylpheed-plugin-0-1.a
```


test.dllがビルドできたら、以下のようにしてプラグインをインストールします。 [^8]

```text
$ cp test.dll /c/Users/(ユーザー名)/AppData/Roaming/Sylpheed/plugins
```


あるいは、zipアーカイブ版の `plugins` ディレクトリにコピーするのでもよいです。

Sylpheed.exeを実行し、アプリケーションを起動します。

メニューにある `[設定] - [プラグインの管理]` から `test` プラグインがリスト登録されているはずです。

### おまけ

SylpheedのWindows版のzipをもとに、Linux上でプラグインをクロスコンパイルすることもできます。

Linuxの場合、pexportsの代替としてgendefというmingw-w64-toolsパッケージ [^9]に含まれているツールをつかってインポートライブラリを作成します。

```text
% gendef libsylph-0-1.dll
```


これでインポートライブラリができたら、コンパイルしましょう。SDKに含まれる.pcファイルはプレフィクスとして/targetを前提としているのでシンボリックリンクを張っておくとよいでしょう。[^10]

ここまで準備できたら、あとは次の手順でビルドするだけです。

```text
% i686-w64-mingw32-gcc -c test.c -I../../libsylph -I../../src `pkg-config --cflags glib-2.0` `pkg-config --cflags gtk+-2.0`
% i686-w64-mingw32-gcc -shared -o test.dll test.c -I../../libsylph -I../../src `pkg-config --cflags gtk+-2.0` `pkg-config --libs gtk+-2.0` $HOME/work/sylpheed/mingw/Sylpheed-3.4.3/libsylph-0-1.dll.a $HOME/work/sylpheed/mingw/Sylpheed-3.4.3/libsylpheed-plugin-0-1.dll.a
```


test.dllをWindowsのプラグインディレクトリへとコピーしてあげれば、動作します。

### まとめ

Windows版のSylpheed 3.4.3向けにプラグインを作る方法を紹介しました。
Windows版は長らくバンドルしているGTK+のバージョンが古く、公式版に合わせたプラグインを開発しようとすると新しめの便利なGLib/GTK+のAPIがことごとく使えないという問題がありましたが、現在ではそれも解消されています。

一度プラグインを作る環境さえ整えてしまえば、Windowsであっても[^11]プラグインを作るのはそれほど難しいことではありません。
`test` プラグインよりも、もう少しだけ複雑なものについては、[Sylpheedのプラグインの作り方]({% post_url 2013-03-26-index %})で紹介しています。
「メール送信時に本文のプレビューと、送信可否の問い合わせを行うプラグイン」として、サンプルコードをまじえて解説しています。

これからはじめる人は参考にしてみてください。

[^0]: この記事を書くきっかけは、IMAP IDLE/NOTIFYに対応したsylpheed-imap-notifyというプラグインで、Windows版が欲しいと言っている人がいたため。実際にはプラグインが非公開のAPIを使っていたため、Windows版の提供は難しいことがわかった。

[^1]: インタフェースバージョンのことを指す。

[^2]: Windows版特有の内容を除いては、Linuxなどでもその知識を生かしてプラグインを開発することができます。

[^3]: インストーラ版およびzipアーカイブ版にはソースは含まれていません。

[^4]: アイコン等が変わらないので、添付ファイルが削除されていることは添付ファイルのサイズ等から判断します。

[^5]: プラグインを作りたいだけなら、ソースからビルドする必要はない。独自ビルドする必要があるのは、エクスポートされていないシンボルをプラグインからどうしても使いたいときに限られる。

[^6]: すでにお使いのzipアーカイブ版もしくはインストール版のバイナリがあれば、改めてダウンロードしなくても既存のものを使うのでも構いません。その場合は適宜読み替えてください。

[^7]: pkg-configはGTK+に含まれています。

[^8]: ユーザー名は各自読み替えてください。

[^9]: Debianの場合。他のディストリビューションの場合は適宜読みかえてください。

[^10]: atk.pcだけ/devel以下を前提としているので、その場合修正する必要があります。

[^11]: GTK+の知識さえあればという条件がつく
