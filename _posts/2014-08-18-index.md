---
tags:
- clear-code
title: 新装版リファクタリング
---
2014年7月にリファクタリングの新装版がオーム社から出版されました。旧版は2000年にピアソン・エデュケーションから出版されていましたが、今は絶版となっています。
<!--more-->


[https://amazon.co.jp/dp/9784274050190](https://amazon.co.jp/dp/9784274050190)

### 内容

訳は洗練されたそう[^0]ですが、翻訳対象の内容は2000年のものと変わっていません。旧版のバージョンアップではなくリニューアルです。

本書を読むと、当時は「リファクタリング」は一般的ではなかったことがわかります。リファクタリングの方法の説明だけでなく、どうやって広めていこうか、といった話がちらほらと入っているからです。

10数年経った今、リファクタリングは「当たり前」になっています。最先端の開発者[^1]だけでなく、ちょっとした開発者であれば知っています。「あぁ、リファクタリングしたい」なんて普通に言っています。

「コードをキレイにすることでしょ？」ちょっとした開発者が知っていることはそんな感じかもしれません。旧版が絶版になっていることもあり、リファクタリングという本を読んだことがない開発者は増えているかもしれません。

リファクタリングという本が伝えようとしていることは「コードを*安全に*改善すること」です。単に、改善すること、キレイにすること、を伝えているわけではありません。「*安全に*」がポイントです。

本書の大部分を占めるのはリファクタリング（改善方法）のカタログです。カタログは次のものを含んでいます。

  * 名前
  * 要約
  * 動機
  * 手順
  * 例

本書を読むと「手順」がとてもしっかりしていることに気づきます。この通りやっていけばたしかに「*安全に*」既存の機能を壊すことなく改善できるな、とわかります。単に改善することが大事ならば、もっと簡単に説明したり、ポイントだけを指摘することもできたはずです。そうしていないのは「*安全に*」を大事にしているからでしょう。

あなたのリファクタリングは単にコードをキレイにするだけですか？それとも、コードを*安全に*改善していますか？もし、「リファクタリング？コードをキレイにすることでしょ？」くらいに思っているなら本書を読んでみるべきです。「なぜリファクタリングをするのか」、そのために「どうやってリファクタリングをするのか」を整理する機会になるはずです。自分自身のコードに対する考えのリファクタリングですね。「リファクタリングをするとコードが改善される」というあなたの挙動は変わらないでしょうが、考えはずっと明確になるはずです。

### テストとリファクタリングとリーダブルコード

本書の中でも自動化されたテストについて触れています。テストがあった上でリファクタリングができるといいます[^2]。その後、10年ほど経ってリーダブルコードが出版されました。

リーダブルコードの観点で見るとリファクタリングの例にでてくるコードは微妙に見えることもあるでしょう。リーダブルコードでは`tmp`や`result`などの一般的な名前よりも、もっと具体的な名前を使おうといいます。一方、リファクタリングでは`result`を積極的に使っています[^3]。

今はリファクタリングよりもリーダブルコードが広く読まれています。リーダブルコードを読んで、実践して、身につけた最近の人たちは、次のステップとしてテスト、その次のステップとしてリファクタリングに進んでみるとよいでしょう。

リーダブルコードにするために自己流で改善していたやり方が体系化され、整理されることでしょう。リーダブルコードが示しているのは改善する指針だけです。リファクタリングは具体的な改善手順まで示しています。そのまま鵜呑みにできるものもあればそうでないものもあるはず[^4]ですが、自分のやり方を体系化するヒントにはなるはずです。

クリアコードも、SEゼミでやったリーダブルコード勉強会の次のステップとして、テストやリファクタリングも伝えていくのはどうかと検討しています。なお、まだ正式アナウンスはでていませんが、10月30日にアジャイルアカデミーでリーダブルコードを題材にした有料セミナーを開催する予定です。リーダブルコードやリファクタリングなどを活用してメンテナンス性の高いコードを開発したい方は予定を空けておいてください。

[^0]: 旧版と比べていないので違いはわかっていません。

[^1]: 最先端の開発者ってなんだろう？

[^2]: 「堅実なテストは、リファクタリングをする上で欠かせない事前条件です。」第4章「テストの構築」の最初の文。

[^3]: `result`はMartin Fowlerのいつものコードの書き方のようです。ちなみに、Rubyに標準添付のXMLパーサーREXMLは`rv`を好んで使っています。REXMLのソースをよく読んでいた頃は、真似して`rv`を使っていたことを思い出しました。

[^4]: リファクタリングはJavaを使っているため、コンパイラーを活用しています。言語によってはこのやり方は使えません。
