---
title: 'IISでリバースプロキシ'
author: abetomo
tags:
- windows
---

普段はLinux + Apache or nginxの阿部です。

Windowsは個人PCでもサーバでも素人です。
そんな私がWindowsサーバでIISを動かし、リバースプロキシの設定をしたので、そのまとめです。
（「IISでリバースプロキシの設定をする」に焦点を当てるため、IISのインストールには触れません。）

IISでリバースプロキシの設定したい方の参考になると思います。

<!--more-->

### はじめに

IISのインストールなど「IISでリバースプロキシの設定」をするための準備は整っている前提の記事です。

以下の順番で説明していきます。

1. 必要なモジュールのインストール
2. 基本的なリバースプロキシの設定
3. いくつか具体例の紹介

Windowsサーバは日本語の設定がしてあります。他の言語でご利用の場合は適宜読み替えてください。

### 1. 必要なモジュールのインストール

IIS自身にはリバースプロキシをする機能がないため、実現するために追加のモジュールを2つインストールします。

* Application Request Routing（ARR）
  * https://www.microsoft.com/en-us/download/details.aspx?id=47333
* URL Rewrite（URL 書き換え）
  * https://www.iis.net/downloads/microsoft/url-rewrite
    * ページの下の方にダウンロードURLがあります

それぞれインストーラーをダウンロードして実行すればインストールできます。
カスタマイズ要素はないので、ポチポチ進めていけばインストールできると思います。

インストールが終わると「インターネット インフォメーション サービス（IIS）マネージャー」でコンピュータレベルの IIS カテゴリに追加されます。

![スクリーンショット：IISマネージャー]({% link /images/blog/reverse-proxy-with-iis/iis-manager.png %})

### 2. 基本的なリバースプロキシの設定

ユーザが`http://iis-web-server/backend-tool/`のように`/backend-tool`というパスへアクセスしたときに、バックエンドサーバ（URL: `http://localhost:4321`）へ転送するときの設定方法を説明します。

「A. パスごとにアプリケーションを追加する方法」と「B. アプリケーションは1つでURL 書き換えで全部やる」の2つの方法を説明します。

パスごとに設定が分かれるので「A. パスごとにアプリケーションを追加する方法」のほうがおすすめです。

#### A. パスごとにアプリケーションを追加する方法

##### A-1. アプリケーションの追加

アプリケーションを追加します。アプリケーション = パスと考えてよいです。

「Default Web Site」へ設定する例です。
まず、サイト > Default Web Site で右クリックをしてメニューを開き「アプリケーションの追加」をクリックします。
設定画面が開くので設定します。

![スクリーンショット：アプリケーションの追加]({% link /images/blog/reverse-proxy-with-iis/add-application.png %})

* エイリアス
  * パスになるので、設定したいパスを指定
  * 例では`backend-tool`を指定
* 物理パス
  * 本来であればドキュメントルートを指定
  * 今回はリバースプロキシでHTMLファイルなどは参照しないので、どのフォルダでもよい
  * ただし、設定した物理パスに `web.confg` という設定ファイルが自動生成され、それをIISが読み書きするので、IISが読み書きできるフォルダを指定
  * 例では`C:\inetpub\wwwroot\backend-tool`を指定

「OK」をクリックしてアプリケーションの追加は完了です。次にリバースプロキシの設定をします。

##### A-2. リバースプロキシ設定

追加した`backend-tool`アプリケーションで、「URL 書き換え」をダブルクリックします。

![スクリーンショット：URL 書き換えのクリック]({% link /images/blog/reverse-proxy-with-iis/url-rewrite-click.png %})

「URL 書き換え」の画面が開くので右側にある「操作」メニューから「規則の追加」をクリックします。

![スクリーンショット：規則の追加]({% link /images/blog/reverse-proxy-with-iis/add-rule.png %})

「規則の追加」メニューが開きますので、規則テンプレートの中から「リバースプロキシ」をダブルクリックします。

![スクリーンショット：リバースプロキシテンプレート]({% link /images/blog/reverse-proxy-with-iis/template-reverse-proxy.png %})

表示される「リバースプロキシ規則の追加」メニューで「受信規則」の「HTTP要求が転送されるサーバ名またはIPアドレスを入力してください」に `localhost:4321` （`http://`は不要）を入力し「OK」をクリックすれば設定は完了です。

![スクリーンショット：リバースプロキシ規則の追加]({% link /images/blog/reverse-proxy-with-iis/add-reverse-proxy-rule.png %})

以上で設定が完了したので `http://iis-web-server/backend-tool/` へアクセスすればバックエンドのツールが表示されるはずです。

#### B. アプリケーションは1つでURL 書き換えで全部やる

「A. パスごとにアプリケーションを追加する方法」ではパスごとにアプリケーションを追加しました。

「B. アプリケーションは1つでURL 書き換えで全部やる」ではパスごとにアプリケーションは追加せずに、タイトルの通りURL 書き換えで全部やる方法を説明します。

「Default Web Site」への設定を例に説明します。
「Default Web Site」にある「URL 書き換え」で設定をします。

![スクリーンショット：Default Web SiteのURL 書き換えのクリック]({% link /images/blog/reverse-proxy-with-iis/default-url-rewrite-click.png %})

「A-2. リバースプロキシ設定」と同様に設定をします。

![スクリーンショット：リバースプロキシ規則の追加]({% link /images/blog/reverse-proxy-with-iis/add-reverse-proxy-rule.png %})

「Default Web Site」に設定をしたので、この状態だと `/` へのアクセスで、`http://localhost:4321`が表示される設定になっています。
`/backend-tool`へのアクセスで`http://localhost:4321`が表示されるように追加で設定します。

「URL 書き換え」画面の一覧表示に追加した設定が表示されるのでそれをダブルクリックします。

![スクリーンショット：URL 書き換えの一覧]({% link /images/blog/reverse-proxy-with-iis/url-rewrite-list.png %})

「受信規則の編集」メニューが表示されるので、「パターン」の部分を書き換えます。
`(.*)` が設定されているはずなので、 設定したいパスである`backend-tool` を追加した `backend-tool/?(.*)` に書き換えます。

![スクリーンショット：パターン設定]({% link /images/blog/reverse-proxy-with-iis/update-pattern.png %})

以上で設定が完了したので `http://iis-web-server/backend-tool/` へアクセスすればバックエンドのツールが表示されるはずです。

### 3. いくつか具体例の紹介

いくつか具体的なツールを例に設定内容を示します。（前提条件などがあるので、それをご理解の上で設定例を参考にしてください。）

* Redmine
* Kibana
* Metabas
* GitBucket（+ Tomcat）

#### 前提条件・注意

リバースプロキシだとツールごとにパスを分けて運用することが多いので、その前提で説明します。

ツールの初期設定は割愛しツールを起動するところからの説明です。パスを分けることで生じる設定については簡単に触れます。

またIISと同じサーバでバックエンドツールが起動しているものとします。
設定例で指定しているポート番号はツールのデフォルト値です。
複数のツールを同一サーバ内で起動する場合はポート番号が重複しないように、ポート番号の設定が必要な場合があります。

リバースプロキシの設定はおすすめである「2. 基本的なリバースプロキシの設定 > A. パスごとにアプリケーションを追加する方法」で示します。

#### Redmine

`/redmine` というパスで運用する場合を例にします。

`/redmine` といったパスで運用する場合は、以下のコマンドのように環境変数なりで設定する必要があります。
（この設定だとRedmineが静的ファイルも返します。）

```console
set SCRIPT_NAME=/redmine
set RAILS_RELATIVE_URL_ROOT=/redmine
ruby bin/rails server
```

上述の通り起動したRedmineへリバースプロキシをする場合は、次の設定で「A. パスごとにアプリケーションを追加する方法」に従い設定すれば利用できます。

* A-1. アプリケーションの追加
  * 「エイリアス」に `redmine` を指定
* A-2. リバースプロキシ設定
 * 「HTTP要求が転送されるサーバ名またはIPアドレスを入力してください」に `localhost:3000` を指定

#### Kibana

`/kibana` というパスで運用する場合を例にします。

`/kibana` といったパスで運用する場合は `config/kibana.yml` に `server.basePath: "/kibana"` の設定が必要です。

設定した後にKibanaを起動します。

上述の通りの設定で起動したKibanaへリバースプロキシをする場合は、次の設定で「A. パスごとにアプリケーションを追加する方法」に従い設定すれば利用できます。

* A-1. アプリケーションの追加
  * 「エイリアス」に `kibana` を指定
* A-2. リバースプロキシ設定
 * 「HTTP要求が転送されるサーバ名またはIPアドレスを入力してください」に `localhost:5601` を指定

#### Metabase

`/metabase` というパスで運用する場合を例にします。

Metabaseは追加の設定なしで `/metabase` といったパスで運用できます。

起動したMetabaseへリバースプロキシをする場合は、次の設定で「A. パスごとにアプリケーションを追加する方法」に従い設定すれば利用できます。

* A-1. アプリケーションの追加
  * 「エイリアス」に `metabase` を指定
* A-2. リバースプロキシ設定
 * 「HTTP要求が転送されるサーバ名またはIPアドレスを入力してください」に `localhost:3000` を指定

#### GitBucket（+ Tomcat）

`/gitbucket` というパスで運用する場合を例にします。

GitBucketはWARファイルで提供されているのでTomcatで運用している方も多いのではないでしょうか。
Apacheでリバースプロキシ + TomcatだとAJPというプロトコルが使えるのですが、IISではAJPは使えないのでHTTPで起動しておく必要があります。

HTTPで起動したGitBucket（+ Tomcat）へリバースプロキシをする場合は、次の設定で「A. パスごとにアプリケーションを追加する方法」に従い設定すれば利用できます。

* A-1. アプリケーションの追加
  * 「エイリアス」に `gitbucket` を指定
* A-2. リバースプロキシ設定
 * 「HTTP要求が転送されるサーバ名またはIPアドレスを入力してください」に `localhost:8080/gitbucket` を指定

### まとめ

IIS + Application Request Routing（ARR） + URL 書き換え でシンプルにリバースプロキシの設定をする方法を説明しました。
単純な設定であれば比較的簡単に設定できたのではないでしょうか。

### 補足

IIS + Application Request Routing（ARR） + URL 書き換え でリバースプロキシをした場合、思った通りに動かない場合もあるのでご注意ください。

例: IISでは「Windows認証」ができます。認証に成功するとアカウントIDが環境変数`REMOTE_USER`に設定されます。
しかし、この `REMOTE_USER` はバックエンド側に渡せないという制限があります。
