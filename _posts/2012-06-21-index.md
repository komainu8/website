---
tags: []
title: 札幌Ruby会議2012をPlatinumスポンサーとして後援し、3名に参加チケットを譲ります
---
クリアコードは、[例年、日本Ruby会議をスポンサーとして後援]({% post_url 2011-05-19-index %})していましたが、[札幌Ruby会議2012](http://sapporo.rubykaigi.org/2012/ja/)も[Platinumスポンサーとして後援](http://sapporo.rubykaigi.org/2012/ja/sponsors.html#P05)します。
<!--more-->


さて、Platinumスポンサーになると札幌Ruby会議2012本編の参加チケット（懇親会は含まない）を3枚もらえるのですが、それを3枚とも希望者に譲ります。（クリアコードから参加する人は[全員発表者として参加]({% post_url 2012-06-04-index %})するので、すでにチケットを持っており、スポンサー用のチケットを使う必要がない。）

応募方法などは[日本Ruby会議2011でえにしテックさんが北海道の学生さん1名を招待したときの方法](http://www.enishi-tech.com/news/2011/05/23/rubykigi2011-campaign.html)のように以下の通りとします。

### 概要

株式会社クリアコードは、札幌Ruby会議2012の本編チケットを3名に提供します。（懇親会チケットや会場までの交通費や宿泊費などは各自で別途用意してください。）

### 応募資格

  * プログラミングが好きな人
  * 2012年9月14日（金） - 16日（日）に行われる札幌Ruby会議2012に参加出来る方
  * Twitterの[@_clear_code](http://twitter.com/_clear_code/)からのダイレクトメッセージで当選の通知を受け取れる方（ご自身のblog等にレポートを書いていただける方が望ましいです）

### 応募方法

  1. [@_clear_code](http://twitter.com/_clear_code/)アカウントをフォローしてください。

  1. 8月4日(土) 00:00 JSTまでにTwitterにて[@_clear_code](http://twitter.com/_clear_code/)へ"#sprk2012 行きたい！ http://www.clear-code.com/blog/2012/6/21.html"からはじめて思いの丈を綴り、合わせて140字に収まる範囲でツイートしてください。例: "@_clear_code #sprk2012 行きたい！ http://www.clear-code.com/blog/2012/6/21.html 僕も札幌RubyKaigi2012に参加したいです！" （64文字。短縮URLが使われるため。）


応募多数の場合は厳正な選考を行い、当選者は[@_clear_code](http://twitter.com/_clear_code)および[ククログ](/blog)にて発表いたします。

奮ってご応募ください。
