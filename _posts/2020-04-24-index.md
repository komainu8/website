---
tags:
- mozilla
title: Firefox 74以降でのアドオンのサイドローディングの代替
---
[Mozillaの公式なアナウンス](https://blog.mozilla.org/addons/2020/03/10/support-for-extension-sideloading-has-ended/)にある通り、Firefox 74以降のバージョンではアドオンの「サイドローディング」ができなくなりました。この変更は次のESR版であるESR78にも影響するので、主に企業ユーザー向けの情報として、対策・代替運用の情報をご紹介します。
<!--more-->


### サイドローディングとは何？　なぜ廃止されたの？

まず用語の説明をします。

「サイドローディング」とは、通常の一般的なインストール手順を経ずに、FirefoxやThunderbirdにアドオンをインストールする方法です。Norton Internet Securityなどのソフトウェアをインストールすると、自分でインストールした覚えがないのにFirefoxやGoogle ChromeにNorton Internet Securityとの連携用ツールがインストールされることがありますが、これがサイドローディングです。

一般ユーザーにとっては、サイドローディングはセキュリティ上の脅威やユーザー体験の劣化に繋がるリスクがあります。悪意あるアドオンがサイドローディングされると、機密情報や個人情報の漏洩に繋がりかねません。また、サイドローディングでインストールされたアドオンは一般ユーザー権限では削除できず、Firefoxのアドオンマネージャ上に「削除できないアドオン」として居座り続けることになります。無効化はできますが、それでも毎回の起動時に不要なファイルが多数読み込まれると、起動にかかる時間の増大は避けられません。これらが、Firefox 74で公式にサイドローディングが廃止されるに至った背景です。

### ESRでのサイドローディング

先に紹介したMozillaの公式のアナウンスには、「Additionally, Firefox Extended Support Release (ESR) will continue to support sideloading as an extension installation method.（ESR版では引き続きサイドローディングが使えます）」と書かれています。企業ではアドオンを社内のセキュリティ施策の実現のために使っている事例があるなど、アドオンが業務と密接に関わることがあり、このような判断がなされたようです。

法人利用での目下の課題は、*現在の所ESR78のベータ版や開発版ではサイドローディングを使った運用の検証ができない*という点です。

サイドローディングの有効・無効の切り替えは現在の所[ビルドオプションで行われており、更新チャンネルがESRと指定されている場合にON、そうでなければOFFになる](https://searchfox.org/mozilla-central/rev/41c3ea3ee8eab9ce7b82932257cb80b703cbba67/toolkit/moz.configure#1236)という設計になっています。そのため、更新チャンネルがESRではない開発版やベータ版ではサイドローディングが無効化されたままとなります。ESR78でアドオンをサイドローディングさせた状態の運用を検証するためには、正式版リリースを待つか、ソースコードをダウンロードしてきてESR版として[自分でビルドする]({% post_url 2017-10-12-index %})しかありません[^0]。

このような問題があることに加え、サイドローディングには、ESRであってもいつまで利用できるかは不透明であるという懸念があります。今の所、明確に「ESRでも廃止する」というアナウンスは出されていませんが、将来的には廃止される可能性がある機能と思っておいた方が安全でしょう。

将来のESRやESR78の検証段階ではアドオンをサイドローディングできなくなる事を前提に考えて、企業の情シスご担当の方は、今のうちに*アドオンの管理はポリシー設定で行う運用に移行する*のがおすすめです。

### サイドローディングを使わずに、システム管理者がアドオンのインストール状況を管理する

[Firefox内蔵のポリシー設定機能]({% post_url 2018-05-12-best-practice-about-firefox-esr60-policy-engine %})は順調に拡充が進み、企業でのアドオン運用でよく見られるパターンであれば、以下のようなポリシーでおおむね要求事項をカバーできるようになっています。

  * Firefoxを初めて起動したときから、アドオンが確認なしでインストール・有効化されている。
→[`Extensions.Install`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#extensions)

  * アドオンの任意のインストールを禁止する。
→[`InstallAddonsPermission.Default`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#installaddonspermission)

  * アドオンのアンインストール、無効化を禁止する。
→[`Extensions.Locked`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#extensions)と[`BlockAboutAddons`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#blockaboutaddons)

  * アドオンの自動更新は停止し、情シスで検証済みの版のみを使う。
→[`ExtensionUpdate`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#extensionupdate)

以下、運用上の各場面での設定例をご紹介します。

#### 導入時

Firefoxの運用を開始する時点で上記の例に対応するポリシー設定は、以下のようになります。

```json
{
  "policies": {
    "Extensions": {
      "Install": [
        "\\\\fileserver\\shared\\addon-x-1.0.xpi",
        "\\\\fileserver\\shared\\addon-y-1.0.xpi"
      ],
      "Locked":  [
        "addon-x@example.com",
        "addon-y@example.com"
      ]
    },
    "InstallAddonsPermission": {
      "Default": false
    },
    "BlockAboutAddons": true,
    "ExtensionUpdate": false
  }
}
```


この例では、アドオンのインストールパッケージがSambaサーバーの共有ディレクトリ上に置かれており、UNCパス `\\fileserver\shared` でファイルの位置を参照できる場合を想定しています。JSONファイル内ではバックスラッシュはエスケープ文字となるため、UNCパス内の区切り文字のバックスラッシュがすべて二重になっていることに注意して下さい。[Fx Meta Installerを使ってXPIパッケージをローカルに配置する]({% post_url 2012-11-07-index %})インストール方法を使っている場合や、何らかの資材管理ツールを使っている場合には、以下のように通常のローカルファイルパスを指定することもできます。

```json
{
  "policies": {
    "Extensions": {
      "Install": [
        "C:\\distributed-files\\addon-x-1.0.xpi",
        "C:\\distributed-files\\addon-y-1.0.xpi"
      ],
      ...
    },
    ...
  }
}
```


Active Directoryのグループポリシーを使う場合、[.admx形式のポリシーテンプレート](https://github.com/mozilla/policy-templates/releases)をドメインコントローラに読み込ませます。ポリシーテンプレートの使い方は[先日のDNS over HTTPSに関する解説記事]({% post_url 2020-02-27-index %})で詳しく述べていますので、併せてご参照下さい。

また、macOSでは`defaults`コマンドの書き込み対象plistファイルに`/Library/Preferences/org.mozilla.firefox`を指定して、以下の要領で設定します。

```bash
$ PLIST=/Library/Preferences/org.mozilla.firefox
$ sudo defaults write $PLIST EnterprisePoliciesEnabled -bool TRUE
$ sudo defaults write $PLIST Extensions__Install -array /distributed-files/addon-x-1.0.xpi /distributed-files/addon-y-1.0.xpi
$ sudo defaults write $PLIST Extensions__Locked -array addon-x@example.com addon-y@example.com
$ sudo defaults write $PLIST InstallAddonsPermission__Default -bool FALSE
$ sudo defaults write $PLIST BlockAboutAddons -bool TRUE
$ sudo defaults write $PLIST ExtensionUpdate -bool FALSE
```


#### アドオンの更新

Firefoxは、ポリシー設定の[`Extensions.Install`](https://github.com/mozilla/policy-templates/blob/master/docs/index.md#extensions)に記述されたURLまたはパスが変更されていれば、そのアドオンを再インストールするようになっています。ファイル名にバージョン番号が含まれていれば、以下のように設定を更新するだけで、次回起動時にアドオンが更新される結果となります。

```json
{
  "policies": {
    "Extensions": {
      "Install": [
        "\\\\fileserver\\shared\\addon-x-1.1.xpi",
        "\\\\fileserver\\shared\\addon-y-2.0.xpi"
      ],
      ...
    },
    ...
  }
}
```


言い換えると、このように管理者側でアドオンを更新することを考慮すると、*アドオンのインストールパッケージはファイル名にあらかじめバージョン番号を含めた状態にして設置する必要がある*ということになります。

macOSで`defaults`コマンドを使用する場合は、配列の要素を削除する機能が無いため、以下の要領で配列の値すべてを置き換える必要があります。

```bash
$ sudo defaults write $PLIST Extensions__Install -array /distributed-files/addon-x-1.1.xpi /distributed-files/addon-y-2.0.xpi
```


#### アドオンの削除

アドオンの運用を終了する場合は、`Extensions.Install`と`Extensions.Locked`から項目を削除し、`Extensions.Uninstall`に削除対象のアドオンのIDを列挙します。

```json
{
  "policies": {
    "Extensions": {
      "Install": [
      ],
      "Uninstall":  [
        "addon-x@example.com",
        "addon-y@example.com"
      ],
      "Locked": [
      ]
    },
    ...
  }
}
```


macOSで`defaults`コマンドを使用する場合の注意事項は前項と同様です。すべての項目を削除して配列を空にする場合は、`-array`を指定して値を省略します。

```bash
$ sudo defaults write $PLIST Extensions__Install -array
$ sudo defaults write $PLIST Extensions__Uninstall -array addon-x@example.com addon-y@example.com
$ sudo defaults write $PLIST Extensions__Locked -array
```


### まとめ

Firefox 74でのアドオンのサイドローディング廃止と、法人運用でのその影響、および、サイドローディングを使わずにポリシー設定でアドオンを運用する手順についてご紹介しました。

当社では、お客さまからの技術的なご質問・ご依頼に有償にて対応する[Firefoxサポートサービス](/services/mozilla/menu.html)を提供しています。企業内でのFirefoxの運用でお困りの情シスご担当者さまやベンダーさまは、[お問い合わせフォーム](/contact/)よりお問い合わせください。

[^0]: アドオンが認識された状態での動作を検証するだけならユーザーインストールでも構いません。ここでは「Firefoxを初めて起動した時点でアドオンが読み込まれていること」のように、サイドローディングの性質が重要となる場面の検証を想定しています。
