---
tags:
- ruby
title: groonga関連リリース週間
---
今週は全文検索エンジン[groonga](http://groonga.org/)とその関連ソフトウェアがいろいろリリースされました。
<!--more-->


### リリースされたソフトウェア

以下がリリースされたソフトウェアとそのバージョンです。

<dl>






<dt>






groonga 






[1.2.0](http://groonga.org/docs/news.html)






</dt>






<dd>


全文検索エンジン。一部非互換があるためマイナーバージョンがあがっています。詳細は[groonga-dev MLでのアナウンス](http://sourceforge.jp/projects/groonga/lists/archive/dev/2011-March/000463.html)を参照してください。


</dd>








<dt>






[groonga storage engine](http://mroonga.github.com/)






 






[0.5.0](http://mroonga.github.com/news.html)






</dt>






<dd>


ストレージエンジンとしてgroongaを利用するMySQLのプラグイン。まだベータ版です。このリリースにはMySQL 5.5への対応強化などが含まれています。


</dd>








<dt>






[rroonga](http://groonga.rubyforge.org/#about-rroonga)






 






[1.2.0](http://groonga.rubyforge.org/rroonga/NEWS_ja_rdoc.html)






</dt>






<dd>


groongaをRubyから使えるようにするためのRubyの拡張ライブラリ。groonga 1.2.0対応だけではなく、新機能もあります。詳細は[groonga-dev MLでのアナウンス](http://sourceforge.jp/projects/groonga/lists/archive/dev/2011-April/000478.html)を参照してください。


</dd>








<dt>






[ActiveGroonga](http://groonga.rubyforge.org/#about-active-groonga)






 






[1.0.4](http://groonga.rubyforge.org/activegroonga/NEWS_ja_rdoc.html)






</dt>






<dd>


Rails 3でrroongaを利用するためのRubyライブラリ。Rails 3用のページネーションライブラリ[Kaminari](https://github.com/amatsuda/kaminari)（のViewの部分のみ）に対応しています。これも詳細は[groonga-dev MLでのアナウンス](http://sourceforge.jp/projects/groonga/lists/archive/dev/2011-April/000479.html)を参照してください。


</dd>








<dt>






[ActiveGroonga Fabrication](http://groonga.rubyforge.org/#about-active-groonga-fabrication)






 1.0.0 [NEW!]






</dt>






<dd>


オブジェクト生成ライブラリ[Fabrication](https://github.com/paulelliott/fabrication)にActiveGroongaサポートを追加するライブラリ。gemでインストールできます。


{% raw %}
```
% sudo gem install activegroonga-fabrication
```
{% endraw %}


使うための準備はこれだけです。


{% raw %}
```ruby
require 'active_groonga_fabrication'
```
{% endraw %}


後は、通常のFabricationの使い方と同じです。


</dd>


</dl>

### まとめ

今週リリースされたgroonga関連のソフトウェアを紹介しました。

そういえば、先日、groongaを使ったアプリケーションである[るりまサーチで受賞した第3回フクオカRuby大賞のコミュニティ特別賞]({% post_url 2011-03-08-index %})の賞状が届きました。

![第3回フクオカRuby大賞のコミュニティ特別賞の賞状]({{ "/images/blog/20110401_0.jpg" | relative_url }} "第3回フクオカRuby大賞のコミュニティ特別賞の賞状")
