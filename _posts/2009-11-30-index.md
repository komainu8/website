---
tags:
- ruby
title: '告知: 2009/12/14のRailsセミナーはRailsとActive Directoryについて'
---
来月中旬の12/14（月）に[〜Ruby on Rails Technical Night〜Ruby on Railsセミナー](http://www.contents-one.co.jp/ruby/2009/11/ruby-on-railsruby-technical-night.php)でActive Directoryと連携したRailsアプリケーションの開発方法について話します。セミナーの概要は以下の通りです。都合があう方はぜひお越しください。
<!--more-->


<dl>






<dt>






タイトル






</dt>






<dd>


Railsで作るActive Directoryと連携した社内システム


</dd>








<dt>






日時






</dt>






<dd>


2009年12月14日（月）19時30分〜21時


</dd>








<dt>






場所






</dt>






<dd>


[株式会社オプト会議室](http://www.contents-one.co.jp/company/map.php)


</dd>








<dt>






参加方法






</dt>






<dd>


[atndで申し込み](http://atnd.org/events/2337)


</dd>


</dl>

さも1人で話すように書いていますが、そんなことはなく、[株式会社ローハイド．](http://raw-hide.co.jp/)の吉見さんと[株式会社万葉](http://everyleaf.com/)の河野さんの3人で、3部構成です。運用のお話や開発時のお話をされるようなので、そちらに興味のある方も参加してみてください。

### 内容

内容はタイトルの通りで、RailsアプリケーションからActive Directoryの情報を利用する方法、注意しなければいけない点などについて話します。社内システムなど、すでにActive Directoryを導入している環境で動作するRailsアプリケーションを開発する場合、独自にアカウントを管理するのではなく、Active Directory上のアカウント情報を利用する方が、利用者の使い勝手もよくなりますし、運用者の負担も減ります。そういった場合にどのようにActive Directory上のアカウント情報を利用するのがよいか、ということをコード例も示しながら説明します。

Active Directoryとの接続には、ActiveLdap（参考: [Rubyist Magazine - ActiveLdap を使ってみよう（前編）](http://jp.rubyist.net/magazine/?0027-ActiveLdap)）を使います。つまり、Active DirectoryをLDAPサーバとして使った場合のRailsアプリケーションの作り方とも言えます。よって、この話の内容はOpenLDAPなどActive Directory以外のLDAPサーバにも応用できます。LDAPサーバと連携したRailsアプリケーションに興味がある方にも楽しんでもらえるのではないでしょうか。

### まとめ

来月のRailsセミナーでRailsとActive Directoryについて話すことになったので、それの告知をしました。参加費は無料なので興味のある方はお気軽に参加してみてください。現時点で定員の半分ほど埋まっているようなので、気になる方はお早めにどうぞ。

<dl>






<dt>






タイトル






</dt>






<dd>


Railsで作るActive Directoryと連携した社内システム


</dd>








<dt>






日時






</dt>






<dd>


2009年12月14日（月）19時30分〜21時


</dd>








<dt>






場所






</dt>






<dd>


[株式会社オプト会議室](http://www.contents-one.co.jp/company/map.php)


</dd>








<dt>






参加方法






</dt>






<dd>


[atndで申し込み](http://atnd.org/events/2337)


</dd>


</dl>
