---
tags:
- groonga
title: Heroku用Groongaのビルド方法
---
HerokuにGroongaはインストールされていないので、HerokuでGroongaを使うためにはGroongaをビルドしなければいけません。今回はHerokuでGroongaをビルドする方法を説明します。
<!--more-->


### buildpack

Herokuにインストールされていないソフトウェアを使えるようにする仕組みが[buildpack](https://devcenter.heroku.com/articles/buildpacks)です。GroongaはHerokuにインストールされていないソフトウェアなので、buildpackの仕組みに従う必要があります。

### buildpackの作り方

buildpackの仕組みに従うには次の作業が必要です。

  1. Heroku上でGroongaをビルドする。

  1. ビルドしたGroongaをダウンロードできる場所にアップロードする。

  1. ビルドしたGroongaをダウンロードしてセットアップするbuildpackを作る。

  1. 作ったbuildpackをリリースする。


順に説明します。

#### Heroku上でGroongaをビルドしてアップロード

まず、次の2つを一緒に説明します。詳細は後述しますが、同じ処理の単位で実行しなければいけないからです。

  1. Heroku上でGroongaをビルドする。

  1. ビルドしたGroongaをダウンロードできる場所にアップロードする。


この2つを実現するビルド用のHerokuアプリケーションを作ります。Groongaの場合は[heroku-groonga-builder](https://github.com/groonga/heroku-groonga-builder)というのがそれです。

このHerokuアプリケーションは次のことをします。

  1. Groongaのソースをダウンロード

  1. Groongaをビルド

  1. ビルドしたGroongaをアーカイブ

  1. アーカイブしたGroongaをアップロード


heroku-groonga-builderは何もしないRackアプリケーション（`config.ru`を置いているだけ）で、実際の処理は[Rakefile](https://github.com/groonga/heroku-groonga-builder/blob/master/Rakefile)の中に書いています。

ソースのダウンロード、ビルド、アーカイブの処理は次のようなイメージです。

{% raw %}
```
% curl -O http://packages.groonga.org/source/groonga/groonga-4.0.2.tar.gz
% tar xf groonga-4.0.2.tar.gz
% cd groonga-4.0.2
% ./configure --prefix=$HOME/vendor/groonga
% make
% make install
% cd -
% tar cJf heroku-groonga-4.0.2.tar.xz vendor/groonga
```
{% endraw %}

実際は次のように`rake`を実行して↑の処理を実施しています[^0]。

{% raw %}
```
% heroku run:detached rake
```
{% endraw %}

detachedを使っているのは、Groongaのビルドにとても時間がかかるからです。だいたい6時間くらいかかります[^1]。

アーカイブしたGroongaは同じプロセス内でアップロードする必要があります。なぜなら、プロセスが終了するとビルドしたアーカイブも消えるからです。これが後述すると書いていた同じ処理で実行しなければいけない理由です。

アーカイブしたGroongaはGitHubのreleaseページにアップロードしています。例えば、Groonga 4.0.2の場合は次のページです。

  * [Groonga 4.0.2のreleaseページ](https://github.com/groonga/groonga/releases/tag/v4.0.2)

Heroku上からアップロードするのでGitHubにアクセスするためのAPIキーをHerokuアプリケーションに渡さないといけません。APIキーはセキュリティー上で重要な値です。heroku-groonga-builderではこの情報を環境変数で渡しています。環境変数名は`GITHUB_TOKEN`です。

環境変数で渡すには次の2つの方法があります。

  * `heroku config:set GITHUB_TOKEN=xxx`
  * `heroku run:detached rake GITHUB_TOKEN=xxx`

前者はHerokuの仕組みを使って設定する方法で、後者はRakeの仕組みを使って設定する方法です。前者は`heroku config:get GITHUB_TOKEN`で、後者は`heroku ps`で後から値を確認できるので、安全性という面ではどっちもどっちです。毎回新しいAPIキーを生成してそれを使い、ビルドが終わったら破棄するくらいがよいのではないかという案がありますが、実装はしていません。

APIキーを使ってアップロードする方法にはいくつかあります。Go言語で書かれた[github-release](https://github.com/aktau/github-release)というツールを使う方法や、[Octokit](http://octokit.github.io/)などのライブラリーを使う方法です。

heroku-groonga-builderは以前はgithub-releaseを使っていましたが、今はOctokitを使っています。

#### ダウンロードしてセットアップ

HerokuでビルドしたGroongaができたのでそれを使ってHerokuアプリケーション用の環境をセットアップします。これをするモジュールが[heroku-buildpack-groonga](https://github.com/groonga/heroku-buildpack-groonga)です。これはbuildpackの一種です。

実はheroku-buildpack-groongaの中でGroongaをビルドするという選択肢もあるのですが、そうすると`git push heroku master`する毎にGroongaのビルドのために6時間くらいかかるので現実的ではありません。ビルド済みのGroongaをダウンロードすることで時間を短縮しています。

buildpackは次の3つのプログラムが必須です。すべて新規で実装します。実行ファイルになっていれば実装言語は問いません。bashで実装してもRubyで実装しても構いません。

  * `bin/detect`
  * `bin/compile`
  * `bin/release`

`bin/detect`は、このbuildpackを使おうとしているHerokuアプリケーションがこのbuildpackが要求する事前条件を満たしているかをチェックします。例えば、Rubyのbuildpackは「`Gemfile`が存在すること」が事前条件で、heroku-buildpack-groongaは「`groonga/`ディレクトリーが存在すること」が事前条件です。

`bin/compile`は、Herokuアプリケーションが必要なソフトウェアをインストールします。例えば、heroku-buildpack-groongaならアーカイブを展開します。

`bin/release`は、Herokuアプリケーションが使うデフォルトの設定を返します。例えば、環境変数だったりサービスを起動するコマンドラインなどの設定を返します。heroku-buildpack-groongaは環境変数`PATH`の設定などを返します。

詳細は[公式サイトのドキュメント](https://devcenter.heroku.com/articles/buildpack-api)やheroku-buildpack-groongaの実装を参照してください。なお、heroku-buildpack-groongaはRubyで実装しています。

buildpackの動作確認用Herokuアプリケーションがあると便利です。heroku-buildpack-groongaを使ったHerokuアプリケーションは次のコマンドで作成できます。

{% raw %}
```
% heroku apps:create --buildpack https://github.com/groonga/heroku-buildpack-groonga
```
{% endraw %}

buildpackを修正した場合はHerokuアプリケーションを`git push heroku master`してください。`heroku ps:restart`では新しいbuildpackは動きません。既存のbuildpack実行結果を再利用します。

#### buildpackをリリース

buildpackが動くようになったらリリースします。リリース用のプラグインがあるのでインストールします。

{% raw %}
```
% heroku plugins:install https://github.com/heroku/heroku-buildpacks
```
{% endraw %}

これで`heroku buildpacks:publish`が使えるようになります。`buildpacks:publish`時に指定する名前は`#&#123;グループ&#125;/#&#123;名前&#125;`とする習慣があるようなので、heroku-buildpack-groongaは`groonga/groonga`を使っています。

{% raw %}
```
% heroku buildpacks:publish groonga/groonga
```
{% endraw %}

これでbuildpackをS3からダウンロードできるようになります。URLは次のようになります。

{% raw %}
```
https://codon-buildpacks.s3.amazonaws.com/buildpacks/#{グループ}/#{名前}.tgz
```
{% endraw %}

例えば、`groonga/groonga`ならこうなります。

{% raw %}
```
https://codon-buildpacks.s3.amazonaws.com/buildpacks/groonga/groonga.tgz
```
{% endraw %}

ユーザーにはS3からダウンロードしてもらうようにしましょう。

### まとめ

Heroku用Groongaのビルド方法を説明しました。ビルドするには次の4つの手順があります。

  1. Heroku上でGroongaをビルドする。

  1. ビルドしたGroongaをダウンロードできる場所にアップロードする。

  1. ビルドしたGroongaをダウンロードしてセットアップするbuildpackを作る。

  1. 作ったbuildpackをリリースする。


このうち、前半の2つがheroku-groonga-builderで実施していることで、後半の2つがheroku-buildpack-groongaが実施していることです。

HerokuにインストールされていないソフトウェアをHerokuで使えるようにしたいときの参考にしてください。

### おまけ: heroku-buildpack-rroonga

おまけで、Rroonga用のbuildpackである[heroku-buildpack-rroonga](https://github.com/groonga/heroku-buildpack-rroonga)の作り方についても簡単に説明します。

このbuildpackはGroongaのbuildpackとRubyのbuildpackを一緒に使います。次のようなイメージです。

  * `bin/detect`が呼ばれたとき
    * GroongaのbuildpackとRroongaのbuildpackの`bin/detect`相当の処理をする
  * `bin/compile`が呼ばれたとき
    * GroongaのbuildpackとRroongaのbuildpackをダウンロード
    * それぞれの`bin/compile`を実行
    * それぞれの`bin/release`も実行して結果をマージして保存
  * `bin/release`が呼ばれたとき
    * `bin/compile`のときに保存した`bin/release`をマージした結果をそのまま出力

関連記事: [HerokuでRroongaを使う方法]({% post_url 2014-05-28-index %})

[^0]: 以前は[Vulcan](https://github.com/heroku/vulcan)というツールを使っていましたが、今は`heroku run`を使うように変わっています。

[^1]: 最適化を有効にしてビルドした場合です。メモリー使用量は6GBを超えます。はっきり言ってこれはリソースを使い過ぎています。いつHerokuでビルドできなくなってもおかしくはありません。2つまで無料の1XというDynoで使えるメモリーは512MBで、そのDynoでビルドしていると「Error R14 (Memory quota exceeded)」というメッセージがログに出力されます。ただ、プロセスは殺されないので今は1XというDynoでビルドしています。
